From iris.program_logic Require Export adequacy weakestpre.
From iris.algebra Require Import auth.
From iris.algebra Require Import lib.excl_auth.
From lrust.lang Require Export heap lifting.
From lrust.lang.stbor Require Export stbor_ghost.
From lrust.lang Require Import notation.
From lrust.lang Require Import lang.
Set Default Proof Using "Type".

Class lrustPreG Σ := HeapPreG {
  lrust_preG_irig :> invPreG Σ;
  lrust_preG_heap :> inG Σ (authR heapUR);
  lrust_preG_heap_freeable :> inG Σ (authR heap_freeableUR);
  lrust_preG_shared :> inG Σ (authR sharedUR);
  lrust_preG_active :> inG Σ (authR activeUR);
  lrust_preG_interp :> inG Σ interpR;
}.

Definition lrustΣ : gFunctors :=
  #[invΣ;
    GFunctor (constRF (authR heapUR));
    GFunctor (constRF (authR heap_freeableUR));
    GFunctor (constRF (authR sharedUR));
    GFunctor (constRF (authR activeUR));
    GFunctor (constRF interpR) ].
Instance subG_heapPreG {Σ} : subG lrustΣ Σ → lrustPreG Σ.
Proof. solve_inG. Qed.

Definition lrust_adequacy Σ `{!lrustPreG Σ} e φ :
  (∀ `{!lrustG Σ}, True ⊢ WP e {{ v, ⌜φ v⌝ }}) →
  adequate NotStuck e (MkSt ∅ (MkStborState ∅ 0%nat)) (λ v _, φ v).
Proof.
  intros Hwp; eapply (wp_adequacy _ _); iIntros (??).
  iMod (own_alloc (● to_heap ∅)) as (vγ) "Hvγ".
  { apply (auth_auth_valid (to_heap _)), to_heap_valid. }
  iMod (own_alloc (● (∅ : heap_freeableUR))) as (fγ) "Hfγ";
    first by apply auth_auth_valid.
  iMod (own_alloc (●E (MkStborState ∅ 0%nat : leibnizO _) ⋅ (◯E (MkStborState ∅ 0%nat : leibnizO _)) : interpR)) as (γinterp) "[Hinterp● Hinterp◯]"; first apply excl_auth_valid.
  iMod (own_alloc (● (∅ : activeUR))) as (γactive) "Hactive●";
    first by apply auth_auth_valid.

  set (Hheap := HeapG _ _ _ vγ fγ).
  set (Hstbor := StborG _ _ _ _ γactive γinterp).
  set (Hrust := LRustG _ _ Hheap Hstbor).

  iAssert (|={⊤}=> @stbor_inv _ _ Hstbor)%I with "[Hactive● Hinterp◯]" as "BOR".
  { iApply inv_alloc. iModIntro. iExists (MkStborState ∅ 0%nat).
    rewrite /stbor_interp_frag. iFrame "Hinterp◯".
    iExists ∅.
    rewrite /stbor_stack_auth /to_activeUR fmap_empty. iFrame "Hactive●".
    by iApply big_sepM2_empty. }
  iMod "BOR" as "#BOR".

  iModIntro.
  iExists (λ σ _, heap_ctx σ.(state_mem) ∗
                             stbor_interp_auth σ.(state_stbor) ∗
                             stbor_inv)%I.
  iExists (λ _, True%I).
  iSplitL.
  { iFrame "Hvγ Hinterp● BOR". iExists ∅. by iFrame "Hfγ". }
  by iApply (Hwp Hrust).
Qed.
