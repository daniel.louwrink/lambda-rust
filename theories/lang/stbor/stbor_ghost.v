From iris.algebra Require Import big_op gmap frac agree.
From iris.algebra Require Import gset auth lib.frac_auth lib.excl_auth.
From iris.bi Require Import fractional.
From iris.base_logic Require Export lib.own invariants.
From iris.proofmode Require Export tactics.
From lrust.lang.stbor Require Export stbor_semantics stbor_sim.
From lrust.lang Require Export lang_base.
From lrust.lifetime Require Export lifetime_sig.

(** Ghost state *)
Inductive ghost_item :=
| GUnique (tg : tag)
| GSharedRO (γg : gname)
| GSharedRW (γg : gname)
| GDisabled.

Definition ghost_stack := list ghost_item.

Definition sharedUR : ucmraT := gsetUR tag.
Definition activeUR : ucmraT := gmapUR loc (prodR fracR (agreeR (leibnizO ghost_stack))).

(* The "auth pattern". This is used to allow access to the state interpretation
   while not in a weakest precondition rule (using an invariant). *)
Definition interpR : cmraT := excl_authR (leibnizO stbor_state).

Class stborG Σ := StborG {
  shared_inG :> inG Σ (authR sharedUR);
  active_inG :> inG Σ (authR activeUR);
  interp_inG :> inG Σ interpR;
  (* TODO: Maybe it's not necessary to have both of these names here *)
  stbor_stack_name : gname;
  stbor_inv_name : gname;
}.

Definition to_activeUR : gmap loc ghost_stack → activeUR :=
  fmap (λ v, (1%Qp, to_agree v)).

Section defs.
  Context `{!invG Σ, !stborG Σ}.

  Definition stbor_stack_auth (gstks : gmap loc ghost_stack) : iProp Σ :=
    own stbor_stack_name (● to_activeUR gstks).
  Definition stbor_stack (l : loc) (q : Qp) (gstk : ghost_stack) : iProp Σ :=
    own stbor_stack_name (◯ {[ l := (q, to_agree gstk) ]}).

  Definition stbor_shared_auth (γ : gname) (tgs : gset tag) : iProp Σ :=
    own γ (● tgs).
  Definition stbor_shared' (γ : gname) (tgs : gset tag) : iProp Σ :=
    own γ (◯ tgs).
  Definition stbor_shared (γ : gname) (tg : tag) : iProp Σ :=
    stbor_shared' γ {[ tg ]}.

  Global Instance stbor_shared_pers γ tgs : Persistent (stbor_shared γ tgs).
  Proof. apply _. Qed.

  Global Instance stbor_shared_timeless γ tgs : Timeless (stbor_shared γ tgs).
  Proof. apply _. Qed.

  Definition stbor_ghost_item (it : item) (git : ghost_item) : iProp Σ :=
    match git with
    | GUnique tg => ⌜it = ItUnique tg⌝
    | GSharedRO γg => ∃ tgs, ⌜it = ItSharedRO tgs⌝ ∧ stbor_shared_auth γg tgs
    | GSharedRW γg => ∃ tgs, ⌜it = ItSharedRW tgs⌝ ∧ stbor_shared_auth γg tgs
    | GDisabled => ⌜it = ItDisabled⌝
    end.

  Definition stbor_ghost_stack (stk : stack) (gstk : ghost_stack) : iProp Σ :=
    [∗ list] it; git ∈ stk; gstk, stbor_ghost_item it git.

  Definition stbor_rel_stack (stk : stack) (gstk : ghost_stack) : iProp Σ :=
    ∃ stklog, ⌜stack_rel_stack stk stklog⌝ ∧ stbor_ghost_stack stklog gstk.

  Definition stbor_rel_stacks (stks : gmap loc stack) (gstks : gmap loc ghost_stack) : iProp Σ :=
    [∗ map] stk; gstk ∈ stks; gstks, stbor_rel_stack stk gstk.

  Definition stbor_ctx (stbor : stbor_state) : iProp Σ :=
    ∃ gstks, stbor_rel_stacks stbor.(stbor_stacks) gstks ∗ stbor_stack_auth gstks.

  Global Instance stbor_ghost_item_timeless it git : Timeless (stbor_ghost_item it git).
  Proof. destruct git; apply _. Qed.

  Global Instance stbor_stbor_ghost_stack_timeless stk gstk : Timeless (stbor_ghost_stack stk gstk).
  Proof. apply _. Qed.

  Global Instance stbor_rel_stack_timeless stk gstk : Timeless (stbor_rel_stack stk gstk).
  Proof. apply _. Qed.

  Global Instance stbor_rel_stacks_timeless stks gstks : Timeless (stbor_rel_stacks stks gstks).
  Proof. apply _. Qed.

  Global Instance stbor_ctx_timeless stbor : Timeless (stbor_ctx stbor).
  Proof. apply _. Qed.

  (* "Auth pattern" *)
  Definition stbor_interp_auth (stbor : stbor_state) : iProp Σ :=
    own stbor_inv_name (●E (stbor : leibnizO _)).

  Definition stbor_interp_frag (stbor : stbor_state) : iProp Σ :=
    own stbor_inv_name (◯E (stbor : leibnizO _)).

  Definition stborN := lft_userN .@ "stbor".

  Definition stbor_inv : iProp Σ :=
    inv stborN (∃ stbor, stbor_interp_frag stbor ∗ stbor_ctx stbor).

  Global Instance stbor_interp_auth_timeless stbor : Timeless (stbor_interp_auth stbor).
  Proof. apply _. Qed.

  Global Instance stbor_interp_timeless stbor : Timeless (stbor_interp_auth stbor).
  Proof. apply _. Qed.

  Lemma stbor_interp_create stbor :
    ⊢ |==> ∃ γ, own γ (●E (stbor : leibnizO _)) ∗ own γ (◯E (stbor : leibnizO _)).
  Proof.
    iMod (own_alloc (●E (stbor : leibnizO _) ⋅ (◯E (stbor : leibnizO _)))) as (γ) "[H● H◯]"; first apply excl_auth_valid.
    iModIntro. iExists γ. iFrame "H● H◯".
  Qed.

  Lemma stbor_interp_agree stbor1 stbor2 :
    stbor_interp_auth stbor1 -∗ stbor_interp_frag stbor2 -∗ ⌜stbor1 = stbor2⌝.
  Proof.
    iIntros "H● H◯".
    iDestruct (own_valid_2 with "H● H◯") as %Hvalid.
    iPureIntro. by apply excl_auth_agreeL in Hvalid.
  Qed.

  Lemma stbor_interp_set stbornew stbor1 stbor2 :
    stbor_interp_auth stbor1 -∗ stbor_interp_frag stbor2 ==∗
    stbor_interp_auth stbornew ∗ stbor_interp_frag stbornew.
  Proof.
    iIntros "H● H◯".
    rewrite /stbor_interp_auth /stbor_interp_frag -own_op.
    iApply (own_update_2 with "H● H◯").
    apply excl_auth_update.
  Qed.

  Lemma stbor_inv_set stbor2 stbor1 E :
    ↑stborN ⊆ E →
    stbor_inv -∗
    stbor_interp_auth stbor1 ={E,E∖↑stborN}=∗
    stbor_ctx stbor1 ∗ stbor_interp_auth stbor2 ∗ (stbor_ctx stbor2 ={E∖↑stborN,E}=∗ True).
  Proof.
    iIntros (?) "#BOR Hinterp●".
    iInv stborN as ">Hinterp" "Hcloseinv".
    iDestruct "Hinterp" as (stbor') "[Hinterp Hctx]".
    iDestruct (stbor_interp_agree with "Hinterp● Hinterp") as %->.
    iMod (stbor_interp_set stbor2 with "Hinterp● Hinterp") as "[Hinterp● Hinterp]".
    iModIntro. iFrame "Hinterp● Hctx".
    iIntros "Hctx".
    iApply ("Hcloseinv" with "[Hctx Hinterp]").
    iModIntro. iExists stbor2. iFrame "Hctx Hinterp".
  Qed.

  Lemma stbor_inv_acc E :
    ↑stborN ⊆ E →
    stbor_inv ={E,E∖↑stborN}=∗
    ∃ stbor, stbor_ctx stbor ∗ (stbor_ctx stbor ={E∖↑stborN,E}=∗ True).
  Proof.
    iIntros (?) "#BOR".
    iInv stborN as ">Hinterp" "Hcloseinv".
    iDestruct "Hinterp" as (stbor) "[Hinterp Hctx]".
    iModIntro. iExists stbor. iFrame "Hctx".
    iIntros "Hctx".
    iApply ("Hcloseinv" with "[Hctx Hinterp]").
    iModIntro. iExists stbor. iFrame "Hctx Hinterp".
  Qed.

  (* Some of this is copied from gen_heap *)
  Lemma stbor_stack_create :
    ⊢ |==> ∃ γ, own γ (● to_activeUR ∅).
  Proof.
    iMod (own_alloc (● to_activeUR ∅)) as (γ) "H●".
    { by apply auth_auth_valid. }
    iModIntro. iExists γ. iFrame "H●".
  Qed.

  Lemma stbor_stack_singleton_included gstks l q gstk :
    {[l := (q, to_agree gstk)]} ≼ to_activeUR gstks → gstks !! l = Some gstk.
  Proof.
    rewrite singleton_included_l=> -[[q' av] []].
    rewrite /to_activeUR lookup_fmap fmap_Some_equiv => -[v' [Hl [/= -> ->]]].
    move=> /Some_pair_included_total_2 [_] /to_agree_included /leibniz_equiv_iff -> //.
  Qed.

  Lemma stbor_stack_lookup l q gstks gstk :
    stbor_stack_auth gstks -∗
    stbor_stack l q gstk -∗
    ⌜gstks !! l = Some gstk⌝.
  Proof.
    iIntros "Hactive● Hactive◯".
    iDestruct (own_valid_2 with "Hactive● Hactive◯") as %Hvalid.
    iPureIntro. apply auth_both_valid in Hvalid as [Hincl _].
    eapply stbor_stack_singleton_included.
    apply Hincl.
  Qed.

  Lemma stbor_stack_alloc l gstk gstks :
    gstks !! l = None →
    stbor_stack_auth gstks ==∗
    stbor_stack_auth (<[l := gstk]> gstks) ∗
    stbor_stack l 1 gstk.
  Proof.
    iIntros (Hfresh) "Hactive●".
    iMod (own_update with "Hactive●") as "[Hactive● Hactive◯]".
    { apply auth_update_alloc, (alloc_singleton_local_update _ l (1%Qp, to_agree (gstk : leibnizO _))); last done.
      rewrite lookup_fmap. by rewrite Hfresh. }
    iModIntro. iFrame "Hactive◯".
    rewrite /stbor_stack_auth -fmap_insert. iFrame "Hactive●".
  Qed.

  Lemma stbor_stack_dealloc l gstks gstk :
    stbor_stack_auth gstks -∗
    stbor_stack l 1 gstk ==∗
    stbor_stack_auth (delete l gstks).
  Proof.
    iIntros "Hactive● Hactive◯".
    iMod (own_update_2 with "Hactive● Hactive◯") as "Hactive●".
    { apply auth_update_dealloc.
      (* For some reason Coq hangs when I try to apply delete_singleton_local_update by itself. *)
      apply (delete_singleton_local_update (to_activeUR gstks) l (1%Qp, to_agree (gstk : leibnizO _))). }
    iModIntro. rewrite -fmap_delete. iFrame "Hactive●".
  Qed.

  Lemma stbor_stack_set l gstk2 gstk1 gstks :
    stbor_stack_auth gstks -∗
    stbor_stack l 1 gstk1 ==∗
    stbor_stack_auth (<[l := gstk2]> gstks) ∗
    stbor_stack l 1 gstk2.
  Proof.
    iIntros "Hactive● Hactive◯".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup.
    rewrite /stbor_stack_auth /stbor_stack -own_op.
    iApply (own_update_2 with "Hactive● Hactive◯").
    apply auth_update.
    rewrite /to_activeUR fmap_insert -/to_activeUR.
    apply (singleton_local_update (to_activeUR gstks) l (1%Qp, to_agree gstk1) (1%Qp, to_agree gstk1) (1%Qp, to_agree gstk2) (1%Qp, to_agree gstk2)).
    { by rewrite lookup_fmap Hlookup. }
    by apply exclusive_local_update.
  Qed.

  Lemma stbor_shared_alloc tgs :
    ⊢ |==> ∃ γg, stbor_shared_auth γg tgs.
  Proof.
    iMod (own_alloc (● tgs)) as (γg) "H●".
    { by apply auth_auth_valid. }
    iModIntro. iExists γg. iFrame "H●".
  Qed.

  Lemma stbor_shared_elem tg tgs γg :
    stbor_shared_auth γg tgs -∗
    stbor_shared γg tg -∗
    ⌜tg ∈ tgs⌝.
  Proof.
    iIntros "Hshared● Hshared◯".
    iCombine "Hshared● Hshared◯" as "Hshared".
    iDestruct (own_valid with "Hshared") as %Hvalid.
    iPureIntro. revert Hvalid.
    rewrite auth_both_valid gset_included -elem_of_subseteq_singleton.
    intros [H _]. apply H.
  Qed.

  Lemma stbor_shared_extend' tg tgs γg :
    stbor_shared_auth γg tgs ==∗
    stbor_shared_auth γg ({[ tg ]} ∪ tgs) ∗
    stbor_shared' γg ({[ tg ]} ∪ tgs).
  Proof.
    iIntros "Hshared●".
    rewrite /stbor_shared_auth /stbor_shared' -own_op.
    iApply (own_update with "Hshared●").
    apply auth_update_alloc, gset_local_update. set_solver.
  Qed.

  Lemma stbor_shared_split tg tgs γg :
    stbor_shared' γg ({[ tg ]} ∪ tgs) -∗
    stbor_shared γg tg.
  Proof.
    iIntros "Hshared".
    rewrite /stbor_shared' -gset_op_union auth_frag_op.
    iDestruct "Hshared" as "[$ _]".
  Qed.

  Lemma stbor_shared_extend tg tgs γg :
    stbor_shared_auth γg tgs ==∗
    stbor_shared_auth γg ({[ tg ]} ∪ tgs) ∗
    stbor_shared γg tg.
  Proof.
    iIntros "Hshared●".
    iMod (stbor_shared_extend' tg with "Hshared●") as "[Hshared● Hshared◯]".
    iModIntro. iFrame "Hshared●".
    iApply (stbor_shared_split with "Hshared◯").
  Qed.

  Lemma stbor_ghost_stack_cons_inv_r stklog git gstk :
    stbor_ghost_stack stklog (git :: gstk) -∗
    ∃ itlog stklog', ⌜stklog = itlog :: stklog'⌝ ∧ stbor_ghost_item itlog git ∗ stbor_ghost_stack stklog' gstk.
  Proof.
    iIntros "Hgstk".
    iDestruct (big_sepL2_cons_inv_r with "Hgstk") as (itlog stklog' Heq) "[Hgit Hgstk]".
    iExists _, _. iFrame "Hgit Hgstk". iPureIntro. assumption.
  Qed.

  Lemma stbor_ghost_stack_app_inv_r stklog gstk1 gstk2 :
    stbor_ghost_stack stklog (gstk1 ++ gstk2) -∗
    ∃ stklog1 stklog2, ⌜stklog = stklog1 ++ stklog2⌝ ∧ stbor_ghost_stack stklog1 gstk1 ∗ stbor_ghost_stack stklog2 gstk2.
  Proof.
    iIntros "Hgstk".
    iDestruct (big_sepL2_app_inv_r with "Hgstk") as (stklog1 stklog2 Heq) "[Hgstk1 Hgstk2]".
    iExists _, _. iFrame "Hgstk1 Hgstk2". iPureIntro. assumption.
  Qed.

  Lemma stbor_ghost_stack_app stklog1 stklog2 gstk1 gstk2 :
    stbor_ghost_stack stklog1 gstk1 -∗
    stbor_ghost_stack stklog2 gstk2 -∗
    stbor_ghost_stack (stklog1 ++ stklog2) (gstk1 ++ gstk2).
  Proof.
    iIntros "Hgstk1 Hgstk2".
    iApply (big_sepL2_app with "[$Hgstk1] [$Hgstk2]").
  Qed.

  Lemma stbor_ghost_stack_singleton itlog git :
    stbor_ghost_item itlog git -∗
    stbor_ghost_stack [itlog] [git].
  Proof.
    iIntros "Hgit".
    iApply big_sepL2_singleton. iFrame "Hgit".
  Qed.

  Lemma stbor_ghost_stack_cons itlog git stklog gstk :
    stbor_ghost_item itlog git -∗
    stbor_ghost_stack stklog gstk -∗
    stbor_ghost_stack (itlog :: stklog) (git :: gstk).
  Proof.
    iIntros "Hgit Hgstk".
    iApply big_sepL2_cons. iFrame "Hgit Hgstk".
  Qed.

  Definition stbor_grants_write (gstk : ghost_stack) (tg : tag) : iProp Σ :=
    match gstk with
    | GUnique tgit :: _ => ⌜tg = tgit⌝
    | GSharedRW γg :: _ => stbor_shared γg tg
    | _ => False
    end.

  Fixpoint stbor_grants_read (gstk : ghost_stack) (tg : tag) : iProp Σ :=
    match gstk with
    | GUnique tgit :: _ => ⌜tg = tgit⌝
    | GSharedRO γg :: gstk => stbor_shared γg tg ∨ stbor_grants_read gstk tg
    | GSharedRW γg :: gstk => stbor_shared γg tg ∨ stbor_grants_read gstk tg
    | GDisabled :: gstk => stbor_grants_read gstk tg
    | _ => False
    end.

  Definition ghost_stack_no_sharedro (gstk : ghost_stack) : Prop :=
    ¬ ∃ γg gstk', gstk = GSharedRO γg :: gstk'.

  Inductive ghost_disable_item : ghost_item → ghost_item → Prop :=
  | ghost_disable_item_keep git : ghost_disable_item git git
  | ghost_disable_item_disable tg : ghost_disable_item (GUnique tg) GDisabled.

  Definition ghost_disable (gstk1 : ghost_stack) (gstk2 : ghost_stack) : Prop :=
    Forall2 ghost_disable_item gstk1 gstk2.

  Inductive ghost_tail_item : ghost_item → Prop :=
  | ghost_tail_item_unique tg : ghost_tail_item (GUnique tg)
  | ghost_tail_item_disabled : ghost_tail_item GDisabled.

  Definition ghost_retag_sharedrw_initial (gstk gstk1 gstk2 : ghost_stack) : Prop :=
    ∃ git gstk2', gstk = gstk1 ++ gstk2 ∧ gstk2 = git :: gstk2' ∧ ghost_tail_item git ∧ ¬ ∃ γg, last gstk1 = Some (GSharedRW γg).

  Definition stbor_grants_retag_sharedrw (gstk : ghost_stack) (γg : gname) (tg : tag) : iProp Σ :=
    ∃ gstk1 git gstk2, ⌜gstk = gstk1 ++ GSharedRW γg :: git :: gstk2⌝ ∧ (⌜git = GUnique tg⌝ ∨ stbor_shared γg tg).

  Global Instance stbor_grants_write_pers gstk tg : Persistent (stbor_grants_write gstk tg).
  Proof. destruct gstk as [|[| | |]]; apply _. Qed.

  Global Instance stbor_grants_write_timeless gstk tg : Timeless (stbor_grants_write gstk tg).
  Proof. destruct gstk as [|[| | |]]; apply _. Qed.

  Global Instance stbor_grants_read_pers gstk tg : Persistent (stbor_grants_read gstk tg).
  Proof. induction gstk as [|[| | |]]; apply _. Qed.

  Global Instance stbor_grants_read_timeless gstk tg : Timeless (stbor_grants_read gstk tg).
  Proof. induction gstk as [|[| | |]]; apply _. Qed.

  Global Instance stbor_grants_retag_sharedrw_pers gstk γg tg : Persistent (stbor_grants_retag_sharedrw gstk γg tg).
  Proof. apply _. Qed.

  Global Instance stbor_grants_retag_sharedrw_timeless gstk γg tg : Timeless (stbor_grants_retag_sharedrw gstk γg tg).
  Proof. apply _. Qed.

  Lemma stbor_grants_write_to_read tg gstk :
    stbor_grants_write gstk tg -∗
    stbor_grants_read gstk tg.
  Proof.
    iIntros "#Hwrite".
    destruct gstk as [|[| | |] gstk]; try done.
    iLeft. iFrame "Hwrite".
  Qed.

  Lemma stbor_grants_write_to_no_sharedro tg gstk :
    stbor_grants_write gstk tg -∗
    ⌜ghost_stack_no_sharedro gstk⌝.
  Proof.
    iIntros "#Hgrants".
    destruct gstk as [|[| | |]]; [done| |done| |];
      iPureIntro; intros (? & ? & H); inversion H.
  Qed.

  Lemma stbor_grants_write_log tg gstk stklog :
    stbor_grants_write gstk tg -∗
    stbor_ghost_stack stklog gstk -∗
    ⌜stack_log_grants_write tg stklog⌝.
  Proof.
    iIntros "#Hgrants Hgstk".
    destruct gstk as [|[| | |] gstk].
    - iExFalso. iApply "Hgrants".
    - iDestruct "Hgrants" as %Hgrants. rewrite -Hgrants.
      iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
      iDestruct "Hgit" as %Hgit. rewrite Hgit.
      iPureIntro. constructor. constructor.
    - iExFalso. iApply "Hgrants".
    - rewrite /stbor_grants_write.
      iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
      iDestruct "Hgit" as (tgs ->) "Hgit".
      iDestruct (stbor_shared_elem with "Hgit Hgrants") as %Helem.
      iPureIntro. constructor. constructor. apply Helem.
    - iExFalso. iApply "Hgrants".
  Qed.

  Lemma stbor_grants_read_log tg gstk stklog :
    stbor_grants_read gstk tg -∗
    stbor_ghost_stack stklog gstk -∗
    ⌜stack_log_grants_read tg stklog⌝.
  Proof.
    iIntros "#Hgrants Hgstk".
    iInduction gstk as [|git gstk] "IH" forall (stklog).
    - iExFalso. iApply "Hgrants".
    - destruct git.
      + iDestruct "Hgrants" as %Heq. rewrite -Heq.
        iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
        iDestruct "Hgit" as %Hgit. rewrite Hgit.
        iPureIntro. constructor. constructor.
      + iDestruct "Hgrants" as "[Hgrants|Hgrants]".
        * iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
          iDestruct "Hgit" as (tgs ->) "Hgit".
          iDestruct (stbor_shared_elem with "Hgit Hgrants") as %Helem.
          iPureIntro. constructor. constructor. apply Helem.
        * iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
          iDestruct "Hgit" as (tgs ->) "Hgit".
          iDestruct ("IH" with "Hgrants Hgstk") as %Hgrantslog.
          iPureIntro. apply stack_log_grants_read_skip; first constructor.
          apply Hgrantslog.
      + iDestruct "Hgrants" as "[Hgrants|Hgrants]".
        * iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
          iDestruct "Hgit" as (tgs ->) "Hgit".
          iDestruct (stbor_shared_elem with "Hgit Hgrants") as %Helem.
          iPureIntro. constructor. constructor. apply Helem.
        * iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
          iDestruct "Hgit" as (tgs ->) "Hgit".
          iDestruct ("IH" with "Hgrants Hgstk") as %Hgrantslog.
          iPureIntro. apply stack_log_grants_read_skip; first constructor.
          apply Hgrantslog.
      + iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
        iDestruct "Hgit" as %Hgit. rewrite Hgit.
        iDestruct ("IH" with "Hgrants Hgstk") as %Hgrantslog.
        iPureIntro. apply stack_log_grants_read_skip; first constructor.
        apply Hgrantslog.
  Qed.

  Lemma stbor_grants_retag_sharedrw_log gstk γg tg stklog :
    stbor_grants_retag_sharedrw gstk γg tg -∗
    stbor_ghost_stack stklog gstk -∗
    ⌜stack_log_grants_retag_sharedrw tg stklog⌝.
  Proof.
    iIntros "#Hgrants Hgstk".
    iInduction gstk as [|git gstk] "IH" forall (stklog).
    { iDestruct "Hgrants" as (gstk1 git gstk2) "[Heq _]".
      iDestruct "Heq" as %Heq. iPureIntro.
      apply app_cons_not_nil in Heq. contradiction. }
    iDestruct "Hgrants" as (gstk1 git' gstk2) "[Heq Hgrants]".
    iDestruct "Heq" as %Heq.
    destruct gstk1 as [|git1 gstk1].
    - simpl in Heq. rewrite Heq.
      iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog1 stklog1 ->) "[Hgit1 Hgstk]".
      iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog2 stklog2 ->) "[Hgit2 Hgstk]".
      iDestruct "Hgit1" as (tgs ->) "Hshared".
      iDestruct "Hgrants" as "[->|Hgrants]".
      + iDestruct "Hgit2" as %Hgit2. rewrite Hgit2.
        iPureIntro. constructor. right. by constructor.
      + iDestruct (stbor_shared_elem with "Hshared Hgrants") as %Helem.
        iPureIntro. constructor. left. assumption.
    - rewrite -app_comm_cons in Heq. inversion Heq.
      iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog1 stklog1 ->) "[Hgit Hgstk]".
      iDestruct ("IH" with "[Hgrants] Hgstk") as %Hlog.
      { iModIntro. iExists _, _, _. iSplit; first done. iFrame "Hgrants". }
      iPureIntro. constructor. assumption.
  Qed.

  Lemma ghost_disable_log gstk1 gstk2 stklog1 :
    ghost_disable gstk1 gstk2 →
    stbor_ghost_stack stklog1 gstk1 -∗
    ∃ stklog2, ⌜log_disable stklog1 stklog2⌝ ∗ stbor_ghost_stack stklog2 gstk2.
  Proof.
    iIntros (Hdis) "Hgstk".
    iInduction Hdis as [|? ? ? ? Hdisit] "IH" forall (stklog1).
    - rewrite /stbor_ghost_stack.
      iDestruct (big_sepL2_length with "Hgstk") as %Hlen.
      apply nil_length_inv in Hlen. rewrite Hlen.
      iExists []. iSplit; first (iPureIntro; constructor).
      by rewrite big_sepL2_nil.
    - iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
      iDestruct ("IH" with "Hgstk") as (stklog2') "[Hdis2 Hgstk]".
      iDestruct "Hdis2" as %Hdis2.
      inversion Hdisit; subst.
      + iExists _; iSplit.
        * iPureIntro. constructor; first apply log_disable_item_keep.
          apply Hdis2.
        * rewrite /stbor_ghost_stack.
          iApply big_sepL2_cons. iFrame "Hgit Hgstk".
      + iDestruct "Hgit" as %->.
        iExists _; iSplit.
        * iPureIntro. constructor; first apply log_disable_item_disable.
          apply Hdis2.
        * rewrite /stbor_ghost_stack.
          iApply big_sepL2_cons. by iFrame "Hgstk".
  Qed.

  Lemma ghost_tail_item_stack_is_tail_item it git :
    ghost_tail_item git →
    stbor_ghost_item it git -∗
    ⌜stack_is_tail_item it⌝.
  Proof.
    iIntros (Hgtailit) "Hgit".
    destruct Hgtailit.
    - iDestruct "Hgit" as %->. iPureIntro. constructor.
    - iDestruct "Hgit" as %->. iPureIntro. constructor.
  Qed.

  Lemma stbor_ghost_item_sharedrw_inv_l tgs git :
    stbor_ghost_item (ItSharedRW tgs) git -∗
    ⌜∃ γg, git = GSharedRW γg⌝.
  Proof.
    iIntros "Hgit".
    destruct git.
    - iDestruct "Hgit" as %Heq. inversion Heq.
    - iDestruct "Hgit" as (? Heq) "_". inversion Heq.
    - by iExists _.
    - iDestruct "Hgit" as %Heq. inversion Heq.
  Qed.

  Lemma ghost_stack_last_no_sharedrw stklog gstk :
    ¬ (∃ γg, last gstk = Some (GSharedRW γg)) →
    stbor_ghost_stack stklog gstk -∗
    ⌜¬ (∃ tgs, last stklog = Some (ItSharedRW tgs))⌝.
  Proof.
    iIntros (Hglast) "Hgstk".
    rewrite -head_reverse in Hglast. rewrite -head_reverse.
    iIntros ([tgs Hlast]).
    iDestruct (big_sepL2_reverse_2 with "Hgstk") as "Hgstk".
    destruct (reverse stklog) as [|itlog stklog']; inversion Hlast.
    iDestruct (big_sepL2_cons_inv_l with "Hgstk") as (git gstk' Heq) "[Hgit Hgstk]".
    iDestruct (stbor_ghost_item_sharedrw_inv_l with "Hgit") as %[γg ->].
    rewrite Heq in Hglast.
    exfalso. apply Hglast. by eexists.
  Qed.

  Lemma ghost_retag_sharedrw_initial_log gstk gstk1 gstk2 stklog :
    ghost_retag_sharedrw_initial gstk gstk1 gstk2 →
    stbor_ghost_stack stklog gstk -∗
    ∃ stklog1 stklog2,
      ⌜retag_sharedrw_initial stklog stklog1 stklog2⌝ ∧
      stbor_ghost_stack stklog1 gstk1 ∗ stbor_ghost_stack stklog2 gstk2.
  Proof.
    iIntros (Hretag) "Hgstk".
    iInduction gstk as [|git gstk] "IH" forall (stklog gstk1 Hretag).
    - destruct Hretag as (? & ? & Heq & -> & _).
      apply app_cons_not_nil in Heq. contradiction.
    - destruct Hretag as (git' & gstk2' & Heq1 & -> & Hgtailit & Hglast).
      destruct gstk1 as [|git1 gstk1].
      + simpl in Heq1. inversion Heq1; subst.
        iExists [], stklog.
        iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
        iDestruct (ghost_tail_item_stack_is_tail_item with "Hgit") as %Htailit; first done.
        iSplit.
        { iPureIntro. do 2 eexists; split; last split; last split; [done..|].
          intros (tgs & Hlast). inversion Hlast. }
        iSplit; first by iApply big_sepL2_nil.
        iApply (stbor_ghost_stack_cons with "Hgit Hgstk").
      + rewrite -app_comm_cons in Heq1. inversion Heq1; subst.
        iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (itlog stklog' ->) "[Hgit Hgstk]".
        iDestruct ("IH" with "[] Hgstk") as (stklog1 stklog2 Hretag) "[Hgstk1 Hgstk2]".
        { iPureIntro. do 2 eexists; split; last split; last split; [done..|].
          intros (γg & Hglast'). apply Hglast. exists γg.
          destruct gstk1 as [|]; first inversion Hglast'. apply Hglast'. }
        iDestruct (stbor_ghost_stack_cons with "Hgit Hgstk1") as "Hgstk1".
        rewrite /retag_sharedrw_initial in Hretag.
        destruct Hretag as (itlog' & stklog2' & Heq3 & -> & Htailit & Hlast').
        iDestruct (ghost_stack_last_no_sharedrw with "Hgstk1") as %Hlast2; first done.
        iExists _, _. iFrame "Hgstk2".
        iFrame "Hgstk1".
        iPureIntro.
        do 2 eexists. rewrite -app_comm_cons; repeat split; [|done..].
        by rewrite Heq3.
  Qed.

  (** Allocation *)

  (* Allocation never fails, since we do not check if the location has already
     been used or not (this is checked by the actual heap, not by Stacked
     Borrows). *)
  Lemma stbor_alloc_1 stbor1 l sz :
    ∃ tgnew stbor2, stbor_step stbor1 (StborAllocEv l sz tgnew) stbor2.
  Proof. eexists _, (MkStborState _ _). constructor. Qed.

  Lemma stbor_alloc_ctx l tgnew stbor1 stbor2 :
    stbor_step stbor1 (StborAllocEv l 1 tgnew) stbor2 →
    stbor1.(stbor_stacks) !! l = None →
    stbor_ctx stbor1 ==∗
    stbor_ctx stbor2 ∗
    stbor_stack l 1 [GUnique tgnew].
  Proof.
    iIntros (Hstep Hfresh) "Hinterp".
    iDestruct "Hinterp" as (gstks) "[Hrel Hactive●]".
    iAssert (⌜gstks !! l = None⌝)%I as %Hfresh2.
    { iDestruct (big_sepM2_dom with "Hrel") as %Hdom.
      rewrite -(not_elem_of_dom (D := gset loc)) -Hdom.
      rewrite not_elem_of_dom. iPureIntro. apply Hfresh. }
    iMod (stbor_stack_alloc l [GUnique tgnew] with "Hactive●") as "[Hactive● Hactive◯]"; first done.
    iModIntro. iFrame "Hactive◯".
    iExists _. iFrame "Hactive●".
    inversion Hstep.
    rewrite /stbor_rel_stacks big_sepM2_insert; try done.
    iFrame "Hrel".
    rewrite /stbor_rel_stack.
    iExists [ItUnique (Tagged (stbor_pnext stbor1))].
    iSplit.
    - iPureIntro.
      exists [ItSharedRO ∅; ItSharedRW ∅; ItUnique (Tagged (stbor_pnext stbor1))]; split.
      + constructor. constructor; constructor.
      + constructor. apply stack_sub_tail_drop_one; constructor.
    - rewrite /stbor_ghost_stack.
      iApply big_sepL2_cons; iSplit; done.
  Qed.

  Lemma stbor_alloc l tgnew stbor1 stbor2 E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborAllocEv l 1 tgnew) stbor2 →
    stbor1.(stbor_stacks) !! l = None →
    stbor_inv -∗
    stbor_interp_auth stbor1 ={E}=∗
    stbor_interp_auth stbor2 ∗
    stbor_stack l 1 [GUnique tgnew].
  Proof.
    iIntros (? Hstep Hfresh) "#BOR Hctx".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iMod (stbor_alloc_ctx with "Hinterp") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hactive◯ Hctx".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Writing *)
  Lemma stbor_write_ctx_1 l tg stbor1 gstk q :
    stbor_grants_write gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ stbor2, stbor_step stbor1 (StborWriteEv l tg 1) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_write_log with "Hgrants Hgstk") as %Hgrantslog.
    edestruct write_single_step as [stk' Hstep]; try done.
    iPureIntro.
    eexists (MkStborState _ _). constructor.
    by rewrite /do_write /range_update Hlookup1 /= Hstep /=.
  Qed.

  (* TODO: Move this? *)
  Definition stack_set (l : loc) (stk : stack) (stbor : stbor_state) : stbor_state :=
    {| stbor_stacks := <[l := stk]> stbor.(stbor_stacks); stbor_pnext := stbor.(stbor_pnext) |}.

  Definition stack_lookup (l : loc) (stbor : stbor_state) : option stack :=
    stbor.(stbor_stacks) !! l.

  Lemma stbor_ctx_lookup stbor l q gstk :
    stbor_ctx stbor -∗
    stbor_stack l q gstk -∗
    ⌜∃ stk, stack_lookup l stbor = Some stk⌝.
  Proof.
    iIntros "Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_dom with "Hrel") as %Hdom.
    iPureIntro.
    apply (elem_of_dom_2 (D := gset loc)) in Hlookup2.
    rewrite -Hdom in Hlookup2.
    apply elem_of_dom in Hlookup2 as (stk & Hlookup2).
    exists stk. apply Hlookup2.
  Qed.

  Lemma stbor_ctx_insert_acc_1 l stbor stkold q gstk :
    stack_lookup l stbor = Some stkold →
    stbor_ctx stbor -∗
    stbor_stack l q gstk -∗

    stbor_rel_stack stkold gstk ∗
    stbor_stack l q gstk ∗
    (∀ stknew, stbor_rel_stack stknew gstk -∗ stbor_ctx (stack_set l stknew stbor)).
  Proof.
    iIntros (Hlookup1) "Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    rewrite /stbor_rel_stacks big_sepM2_insert_acc; [|done..].
    iDestruct "Hrel" as "[Hrel1 Hrel]".
    iFrame "Hrel1 Hactive◯".
    iIntros (stknew) "Hrel1". iSpecialize ("Hrel" with "Hrel1").
    rewrite /stack_lookup in Hlookup2. apply insert_id in Hlookup2 as ->.
    iExists _. iFrame "Hactive● Hrel".
  Qed.

  Lemma stbor_ctx_insert_acc_2 gstk2 l stbor stk1 gstk1 :
    stack_lookup l stbor = Some stk1 →
    stbor_ctx stbor -∗
    stbor_stack l 1 gstk1 ==∗

    stbor_rel_stack stk1 gstk1 ∗
    stbor_stack l 1 gstk2 ∗
    (∀ stk2, stbor_rel_stack stk2 gstk2 -∗ stbor_ctx (stack_set l stk2 stbor)).
  Proof.
    iIntros (Hlookup1) "Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iMod (stbor_stack_set l gstk2 with "Hactive● Hactive◯") as "[Hactive● Hactive◯]".
    iModIntro.
    rewrite /stbor_rel_stacks big_sepM2_insert_acc; [|done..].
    iDestruct "Hrel" as "[Hrel1 Hrel]".
    iFrame "Hrel1 Hactive◯".
    iIntros (stk2) "Hrel1". iSpecialize ("Hrel" with "Hrel1").
    iExists _. iFrame "Hactive● Hrel".
  Qed.

  (* TODO: Move inversion lemmas *)
  Lemma write_inv l tg stbor1 stbor2 :
    stbor_step stbor1 (StborWriteEv l tg 1) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      write_single tg stkold = Some stknew ∧
      stbor2 = stack_set l stknew stbor1.
  Proof.
    intros Hstep.
    inversion Hstep as [|? ? ? ? Hwrite| | |].
    rewrite /do_write /range_update in Hwrite.
    apply bind_Some in Hwrite as (stkold & Hlookup & Hwrite).
    apply bind_Some in Hwrite as (stknew & Hwrite1 & Hwrite).
    exists stkold, stknew. repeat split; [done..|].
    inversion Hwrite.
    destruct stbor2 as [stks2 pnext2].
    destruct stbor1 as [stks1 pnext1].
    rewrite /stack_set. f_equal; subst; done.
  Qed.

  Lemma stbor_write_ctx l tg stbor1 stbor2 gstk q :
    stbor_step stbor1 (StborWriteEv l tg 1) stbor2 →
    stbor_grants_write gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗

    stbor_ctx stbor2 ∗
    stbor_stack l q gstk.
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    edestruct write_inv as (stkold & stknew & Hlookup1 & Hwrite1 & Hset); first done.
    iDestruct (stbor_ctx_insert_acc_1 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_write_log with "Hgrants Hgstk") as %Hgrantslog.
    eapply write_single_preserve in Hgrantslog; [|done..].
    iFrame "Hactive◯". rewrite Hset.
    iApply "Hback".
    iExists _. iFrame "Hgstk". done.
  Qed.

  Lemma stbor_write l tg stbor1 stbor2 gstk q E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborWriteEv l tg 1) stbor2 →
    stbor_inv -∗
    stbor_grants_write gstk tg -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l q gstk ={E}=∗

    stbor_interp_auth stbor2 ∗
    stbor_stack l q gstk.
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iDestruct (stbor_write_ctx with "Hgrants Hinterp Hactive◯") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hactive◯ Hctx".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Deallocation *)
  Lemma stbor_dealloc_ctx_1 l tg stbor1 gstk q :
    stbor_grants_write gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ stbor2, stbor_step stbor1 (StborDeallocEv l tg 1) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_write_log with "Hgrants Hgstk") as %Hgrantslog.
    edestruct write_single_step as [stk' Hstep]; try done.
    iPureIntro.
    eexists (MkStborState _ _). constructor.
    by rewrite /do_dealloc /range_update Hlookup1 /= Hstep.
  Qed.

  Definition stack_delete (l : loc) (stbor : stbor_state) : stbor_state :=
    {| stbor_stacks := delete l stbor.(stbor_stacks); stbor_pnext := stbor.(stbor_pnext) |}.

  Lemma dealloc_inv l tg stbor1 stbor2 :
    stbor_step stbor1 (StborDeallocEv l tg 1) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      write_single tg stkold = Some stknew ∧
      stbor2 = stack_delete l stbor1.
  Proof.
    intros Hstep.
    inversion Hstep as [| | |? ? ? ? Hdealloc|].
    rewrite /do_dealloc /range_update in Hdealloc.
    apply bind_Some in Hdealloc as (stkold & Hlookup & Hdealloc).
    apply bind_Some in Hdealloc as (stknew & Hwrite & Hdelete).
    exists stkold, stknew. repeat split; [done..|].
    inversion Hdelete.
    destruct stbor1 as [stks1 pnext1].
    destruct stbor2 as [stks2 pnext2].
    rewrite /stack_delete. f_equal; subst; done.
  Qed.

  Lemma stbor_dealloc_ctx l tg gstk stbor1 stbor2 :
    stbor_step stbor1 (StborDeallocEv l tg 1) stbor2 →
    stbor_grants_write gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l 1 gstk ==∗
    stbor_ctx stbor2.
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iMod (stbor_stack_dealloc with "Hactive● Hactive◯") as "Hactive●".
    iModIntro.
    iExists _. iFrame "Hactive●".
    edestruct dealloc_inv as (stkold & stknew & Hlookup1 & Hwrite & Hdelete); first done.
    destruct stbor1 as [stks1 pnext1].
    destruct stbor2 as [stks2 pnext2].
    inversion Hdelete.
    iDestruct (big_sepM2_delete with "Hrel") as "[_ $]"; eassumption.
  Qed.

  Lemma stbor_dealloc l tg gstk stbor1 stbor2 E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborDeallocEv l tg 1) stbor2 →
    stbor_inv -∗
    stbor_grants_write gstk tg -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l 1 gstk ={E}=∗
    stbor_interp_auth stbor2.
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iMod (stbor_dealloc_ctx with "Hgrants Hinterp Hactive◯") as "Hinterp"; try done.
    iFrame "Hctx".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Reading *)
  Lemma stbor_read_ctx_1 l tg stbor1 gstk q :
    stbor_grants_read gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ stbor2, stbor_step stbor1 (StborReadEv l tg 1) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_read_log with "Hgrants Hgstk") as %Hgrantslog.
    edestruct read_single_step as [stk' Hstep]; try done.
    iPureIntro.
    eexists (MkStborState _ _). constructor.
    by rewrite /do_read /range_update Hlookup1 /= Hstep /=.
  Qed.

  Lemma read_inv l tg stbor1 stbor2 :
    stbor_step stbor1 (StborReadEv l tg 1) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      read_single tg stkold = Some stknew ∧
      stbor2 = stack_set l stknew stbor1.
  Proof.
    intros Hstep.
    inversion Hstep as [| |? ? ? ? Hread| |].
    rewrite /do_read /range_update in Hread.
    apply bind_Some in Hread as (stkold & Hlookup & Hread).
    apply bind_Some in Hread as (stknew & Hread & Hset).
    exists stkold, stknew. repeat split; [done..|].
    destruct stbor1 as [stks1 pnext1].
    destruct stbor2 as [stks2 pnext2].
    inversion Hset.
    rewrite /stack_set. f_equal; subst; done.
  Qed.

  Lemma stbor_read_ctx l tg stbor1 stbor2 gstk q :
    stbor_step stbor1 (StborReadEv l tg 1) stbor2 →
    stbor_grants_read gstk tg -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗

    stbor_ctx stbor2 ∗
    stbor_stack l q gstk.
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    edestruct read_inv as (stkold & stknew & Hlookup & Hread & Hset); first done.
    iDestruct (stbor_ctx_insert_acc_1 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_read_log with "Hgrants Hgstk") as %Hgrantslog.
    eapply read_single_preserve in Hgrantslog; [|done..].
    iFrame "Hactive◯". rewrite Hset.
    iApply "Hback".
    iExists _. iFrame "Hgstk". done.
  Qed.

  Lemma stbor_read l tg stbor1 stbor2 gstk q E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborReadEv l tg 1) stbor2 →
    stbor_inv -∗
    stbor_grants_read gstk tg -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l q gstk ={E}=∗

    stbor_interp_auth stbor2 ∗
    stbor_stack l q gstk.
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iDestruct (stbor_read_ctx with "Hgrants Hinterp Hactive◯") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hctx Hactive◯".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Retagging (Unique) *)
  Lemma stbor_retag_unique_ctx_1 l tgold stbor1 gstk int_mut q :
    stbor_grants_write gstk tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ tgnew stbor2, stbor_step stbor1 (StborRetagEv l tgold [int_mut] (PkRef Mutable) tgnew) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_write_log with "Hgrants Hgstk") as %Hgrantslog.
    edestruct write_single_step as [stk' Hstep]; try done.
    iPureIntro.
    eexists _, (MkStborState _ _). constructor.
    by rewrite /do_retag /range_interior_update Hlookup1 /= /retag_single /= /retag_unique_single /= Hstep.
  Qed.

  Definition stack_tick (stbor : stbor_state) : stbor_state :=
    {| stbor_stacks := stbor.(stbor_stacks); stbor_pnext := S (stbor.(stbor_pnext)) |}.

  Lemma retag_unique_inv l tgold tgnew stbor1 stbor2 int_mut :
    stbor_step stbor1 (StborRetagEv l tgold [int_mut] (PkRef Mutable) tgnew) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      retag_unique_single tgold tgnew stkold = Some stknew ∧
      stbor2 = stack_tick (stack_set l stknew stbor1).
  Proof.
    intros Hstep.
    inversion Hstep as [| | | |? ? ? ? ? ? Hretag].
    rewrite /do_retag /range_update /range_interior_update in Hretag.
    apply bind_Some in Hretag as (stksnew' & Hretag & Htag).
    apply bind_Some in Hretag as (stkold & Hlookup & Hretag).
    apply bind_Some in Hretag as (stknew & Hretag & Hset).
    exists stkold, stknew.
    rewrite /retag_single /= in Hretag.
    inversion Htag.
    repeat split; [done..|].
    inversion Hset.
    destruct stbor1 as [stks1 pnext1]. destruct stbor2 as [stks2 pnext2].
    rewrite /stack_set /stack_tick. f_equal; subst; done.
  Qed.

  Lemma stbor_retag_unique_ctx l tgold tgnew int_mut stbor1 stbor2 gstk :
    stbor_step stbor1 (StborRetagEv l tgold [int_mut] (PkRef Mutable) tgnew) stbor2 →
    stbor_grants_write gstk tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l 1 gstk ==∗

    stbor_ctx stbor2 ∗
    stbor_stack l 1 (GUnique tgnew :: gstk).
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    edestruct retag_unique_inv as (stkold & stknew & Hlookup & Hretag & Hset); first done.
    iMod (stbor_ctx_insert_acc_2 (GUnique tgnew :: gstk) with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iModIntro.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_write_log with "Hgrants Hgstk") as %Hgrantslog.
    eapply retag_unique_single_preserve in Hgrantslog; [|done..].
    iFrame "Hactive◯". rewrite Hset.
    iApply "Hback".
    iExists _. iSplit; first done.
    by iApply (stbor_ghost_stack_cons with "[] Hgstk").
  Qed.

  Lemma stbor_retag_unique l tgold tgnew int_mut stbor1 stbor2 gstk E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborRetagEv l tgold [int_mut] (PkRef Mutable) tgnew) stbor2 →
    stbor_inv -∗
    stbor_grants_write gstk tgold -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l 1 gstk ={E}=∗

    stbor_interp_auth stbor2 ∗
    stbor_stack l 1 (GUnique tgnew :: gstk).
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iMod (stbor_retag_unique_ctx with "Hgrants Hinterp Hactive◯") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hctx Hactive◯".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Retagging (SharedRO) *)
  Lemma stbor_retag_sharedro_add_ctx_1 l tgold stbor1 gstk q :
    stbor_grants_read gstk tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ tgnew stbor2, stbor_step stbor1 (StborRetagEv l tgold [OutsideUnsafeCell] (PkRef Immutable) tgnew) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_read_log with "Hgrants Hgstk") as %Hgrantslog.
    edestruct read_single_step as [stk' Hstep]; try done.
    iPureIntro.
    eexists _, (MkStborState _ _). constructor.
    by rewrite /do_retag /range_interior_update Hlookup1 /= /retag_single /= /retag_sharedro_single /= Hstep.
  Qed.

  Lemma retag_sharedro_inv l tgold tgnew stbor1 stbor2 :
    stbor_step stbor1 (StborRetagEv l tgold [OutsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      retag_sharedro_single tgold {[ tgnew ]} stkold = Some stknew ∧
      stbor2 = stack_tick (stack_set l stknew stbor1).
  Proof.
    intros Hstep.
    inversion Hstep as [| | | |? ? ? ? ? ? Hretag].
    rewrite /do_retag /range_update /range_interior_update in Hretag.
    apply bind_Some in Hretag as (stksnew' & Hretag & Htag).
    apply bind_Some in Hretag as (stkold & Hlookup & Hretag).
    apply bind_Some in Hretag as (stknew & Hretag & Hset).
    exists stkold, stknew. inversion Htag.
    repeat split; [done..|].
    destruct stbor1 as [stks1 pnext1]. destruct stbor2 as [stks2 pnext2].
    rewrite /stack_set /stack_tick. inversion Hset.
    f_equal; subst; done.
  Qed.

  Lemma stbor_retag_sharedro_add_ctx l tgold tgnew stbor1 stbor2 q γg gstk :
    stbor_step stbor1 (StborRetagEv l tgold [OutsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    stbor_grants_read (GSharedRO γg :: gstk) tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q (GSharedRO γg :: gstk) ==∗

    stbor_ctx stbor2 ∗
    stbor_stack l q (GSharedRO γg :: gstk) ∗
    stbor_shared γg tgnew.
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    edestruct retag_sharedro_inv as (stkold & stknew & Hlookup & Hretag & Hset); first done.
    iDestruct (stbor_ctx_insert_acc_1 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_read_log with "Hgrants Hgstk") as %Hgrantslog.
    iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (it stklog' ->) "[Hgit Hgstk]".
    iDestruct "Hgit" as (tgs ->) "Hgit".
    eapply retag_sharedro_single_extend_preserve in Hgrantslog; [|done..].
    iFrame "Hactive◯". rewrite Hset.
    iMod (stbor_shared_extend tgnew with "Hgit") as "[Hgit $]".
    iModIntro. iApply "Hback".
    iExists _. iSplit; first done.
    iApply (stbor_ghost_stack_cons with "[Hgit] Hgstk").
    iExists _. iFrame "Hgit". done.
  Qed.

  Lemma stbor_retag_sharedro_add l tgold tgnew stbor1 stbor2 q γg gstk E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborRetagEv l tgold [OutsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    stbor_inv -∗
    stbor_grants_read (GSharedRO γg :: gstk) tgold -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l q (GSharedRO γg :: gstk) ={E}=∗

    stbor_interp_auth stbor2 ∗
    stbor_stack l q (GSharedRO γg :: gstk) ∗
    stbor_shared γg tgnew.
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iMod (stbor_retag_sharedro_add_ctx with "Hgrants Hinterp Hactive◯") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hactive◯ Hctx".
    iApply ("Hclose" with "Hinterp").
  Qed.

  (** Enable read-only sharing *)

  Lemma ghost_stack_no_sharedro_rel stklog gstk :
    ghost_stack_no_sharedro gstk →
    stbor_ghost_stack stklog gstk -∗
    ⌜stack_log_no_sharedro stklog⌝.
  Proof.
    iIntros (Hnoshared) "Hgstk".
    destruct gstk as [|git gstk].
    - iDestruct (big_sepL2_length with "Hgstk") as %Hlen.
      apply length_zero_iff_nil in Hlen. rewrite Hlen.
      iPureIntro. intros (? & ? & Heq).
      inversion Heq.
    - destruct git as [| | |].
      + iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (it stklog' ->) "[Hgit Hgstk]".
        iDestruct "Hgit" as %Hgit. rewrite Hgit. iPureIntro.
        intros (? & ? & Heq). inversion Heq.
      + iPureIntro. exfalso. apply Hnoshared.
        by eexists _, _.
      + iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (it stklog' ->) "[Hgit Hgstk]".
        iDestruct "Hgit" as (tgs) "[Hgit _]".
        iDestruct "Hgit" as %Hgit.
        rewrite Hgit. iPureIntro.
        intros (? & ? & Heq). inversion Heq.
      + iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (it stklog' ->) "[Hgit Hgstk]".
        iDestruct "Hgit" as %Hgit. rewrite Hgit. iPureIntro.
        intros (? & ? & Heq). inversion Heq.
  Qed.

  Lemma stack_set_id l stbor stk :
    stack_lookup l stbor = Some stk →
    stack_set l stk stbor = stbor.
  Proof.
    intros Hlookup.
    destruct stbor as [stks pnext].
    rewrite /stack_lookup in Hlookup.
    apply insert_id in Hlookup.
    rewrite /stack_set. f_equal; subst; done.
  Qed.

  Lemma stbor_push_sharedro_ctx l gstk stbor :
    ghost_stack_no_sharedro gstk →
    stbor_ctx stbor -∗
    stbor_stack l 1 gstk ==∗

    ∃ γg,
    stbor_ctx stbor ∗
    stbor_stack l 1 (GSharedRO γg :: gstk).
  Proof.
    iIntros (Hnoshared) "Hctx Hactive◯".
    iDestruct (stbor_ctx_lookup with "Hctx Hactive◯") as %[stk Hlookup].
    iMod (stbor_shared_alloc ∅) as (γg) "Hshared●".
    iMod (stbor_ctx_insert_acc_2 (GSharedRO γg :: gstk) with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iModIntro. iExists γg. iFrame "Hactive◯".
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (ghost_stack_no_sharedro_rel with "Hgstk") as %Hnoshared'; first assumption.
    apply retag_sharedro_single_initial_preserve in Hrel; last done.
    iSpecialize ("Hback" $! stk).
    rewrite stack_set_id; last done.
    iApply "Hback".
    iExists _. iSplit; first done.
    iApply (stbor_ghost_stack_cons with "[Hshared●] Hgstk").
    iExists _. by iFrame "Hshared●".
  Qed.

  Lemma stbor_push_sharedro l gstk E :
    ↑stborN ⊆ E →
    ghost_stack_no_sharedro gstk →
    stbor_inv -∗
    stbor_stack l 1 gstk ={E}=∗

    ∃ γg,
    stbor_stack l 1 (GSharedRO γg :: gstk).
  Proof.
    iIntros (? Hnoshared) "#BOR Hactive◯".
    iMod (stbor_inv_acc with "BOR") as (stbor) "[Hopen Hclose]"; first done.
    iMod (stbor_push_sharedro_ctx with "Hopen Hactive◯") as (γg) "[Hopen Hactive◯]"; first done.
    iExists γg. iFrame "Hactive◯".
    iApply ("Hclose" with "Hopen").
  Qed.

  (** Ghost popping *)
  Lemma stack_ghost_sublist gstk1 gstk2 stklog1 :
    gstk2 `suffix_of` gstk1 →
    stbor_ghost_stack stklog1 gstk1 -∗
    ∃ stklog2, ⌜stklog2 `suffix_of` stklog1⌝ ∧ stbor_ghost_stack stklog2 gstk2.
  Proof.
    iIntros (Hsuffix) "Hgstk".
    iInduction gstk1 as [|it1 gstk1] "IH" forall (stklog1).
    - apply suffix_nil_inv in Hsuffix. rewrite Hsuffix.
      iExists []; iSplit; last by iApply big_sepL2_nil.
      iPureIntro. apply suffix_nil.
    - destruct gstk2 as [|it2 gstk2].
      { iExists []; iSplit; last by iApply big_sepL2_nil.
        iPureIntro. apply suffix_nil. }
      apply suffix_cons_inv in Hsuffix as [Heq|Hsuffix].
      + inversion Heq. iExists stklog1; iSplit; last iFrame "Hgstk".
        iPureIntro. done.
      + iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk") as (it stklog ->) "[Hgit Hgstk]".
        iDestruct ("IH" $! Hsuffix stklog with "Hgstk") as (stklog2) "[Hsuffix' Hgstk']".
        iDestruct "Hsuffix'" as %Hsuffix'.
        iExists _; iSplit; last done.
        iPureIntro. by apply suffix_cons_r.
  Qed.

  Lemma stbor_remove_ctx l stbor gstk1 gstk2 :
    gstk2 `suffix_of` gstk1 →
    stbor_ctx stbor -∗
    stbor_stack l 1 gstk1 ==∗
    stbor_ctx stbor ∗
    stbor_stack l 1 gstk2.
  Proof.
    iIntros (Hsub) "Hctx Hactive◯".
    iDestruct (stbor_ctx_lookup with "Hctx Hactive◯") as %[stk Hlookup].
    iMod (stbor_ctx_insert_acc_2 gstk2 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iModIntro. iFrame "Hactive◯".
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stack_ghost_sublist with "Hgstk") as (stklog' Hsub') "Hgstk"; first done.
    eapply log_pop_prefix_preserve in Hrel; last done.
    iSpecialize ("Hback" $! stk).
    rewrite stack_set_id; last done.
    iApply "Hback".
    iExists _. iSplit; first done.
    iFrame "Hgstk".
  Qed.

  Lemma stbor_remove gstk2 l gstk1 E :
    ↑stborN ⊆ E →
    gstk2 `suffix_of` gstk1 →
    stbor_inv -∗
    stbor_stack l 1 gstk1 ={E}=∗
    stbor_stack l 1 gstk2.
  Proof.
    iIntros (? Hsub) "#BOR Hactive◯".
    iMod (stbor_inv_acc with "BOR") as (stbor) "[Hopen Hclose]"; first done.
    iMod (stbor_remove_ctx with "Hopen Hactive◯") as "[Hopen Hactive◯]"; first done.
    iFrame "Hactive◯".
    iApply ("Hclose" with "Hopen").
  Qed.

  (** * Ghost disabling *)

  Lemma stbor_disable_ctx gstk2 l gstk1 stbor :
    ghost_disable gstk1 gstk2 →
    stbor_ctx stbor -∗
    stbor_stack l 1 gstk1 ==∗
    stbor_ctx stbor ∗
    stbor_stack l 1 gstk2.
  Proof.
    iIntros (Hsub) "Hctx Hactive◯".
    iDestruct (stbor_ctx_lookup with "Hctx Hactive◯") as %[stk Hlookup].
    iMod (stbor_ctx_insert_acc_2 gstk2 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iModIntro. iFrame "Hactive◯".
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (ghost_disable_log with "Hgstk") as (stklog' Hsub') "Hgstk"; first done.
    eapply log_disable_preserve in Hrel; last done.
    iSpecialize ("Hback" $! stk).
    rewrite stack_set_id; last done.
    iApply "Hback".
    iExists _. iSplit; first done.
    iFrame "Hgstk".
  Qed.

  Lemma stbor_disable gstk2 gstk1 l E :
    ↑stborN ⊆ E →
    ghost_disable gstk1 gstk2 →
    stbor_inv -∗
    stbor_stack l 1 gstk1 ={E}=∗
    stbor_stack l 1 gstk2.
  Proof.
    iIntros (? Hdis) "#BOR Hactive◯".
    iMod (stbor_inv_acc with "BOR") as (stbor) "[Hopen Hclose]"; first done.
    iMod (stbor_disable_ctx with "Hopen Hactive◯") as "[Hopen Hactive◯]"; first done.
    iFrame "Hactive◯".
    iApply ("Hclose" with "Hopen").
  Qed.

  (** * Retag (SharedRW) initial *)

  Lemma retag_sharedrw_initial_preserve_ctx l gstk gstk1 gstk2 stbor :
    ghost_retag_sharedrw_initial gstk gstk1 gstk2 →
    stbor_ctx stbor -∗
    stbor_stack l 1 gstk ==∗
    ∃ γg,
      stbor_stack l 1 (gstk1 ++ GSharedRW γg :: gstk2) ∗
      stbor_ctx stbor.
  Proof.
    iIntros (Hretag) "Hctx Hactive◯".
    iDestruct (stbor_ctx_lookup with "Hctx Hactive◯") as %[stk Hlookup].
    iMod (stbor_shared_alloc ∅) as (γg) "Hshared●".
    iMod (stbor_ctx_insert_acc_2 (gstk1 ++ GSharedRW γg :: gstk2) with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iModIntro. iExists γg. iFrame "Hactive◯".
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (ghost_retag_sharedrw_initial_log with "Hgstk") as (stklog1 stklog2 Hretaglog) "[Hgstk1 Hgstk2]"; first done.
    eapply retag_sharedrw_single_initial in Hrel; last done.
    iSpecialize ("Hback" $! stk).
    rewrite stack_set_id; last done.
    iApply "Hback".
    iExists _. iSplit; first done.
    iApply (stbor_ghost_stack_app with "Hgstk1 [Hshared● Hgstk2]").
    iApply (stbor_ghost_stack_cons with "[Hshared●] Hgstk2").
    iExists ∅. by iFrame "Hshared●".
  Qed.

  Lemma retag_sharedrw_initial_preserve l gstk gstk1 gstk2 E :
    ↑stborN ⊆ E →
    ghost_retag_sharedrw_initial gstk gstk1 gstk2 →
    stbor_inv -∗
    stbor_stack l 1 gstk ={E}=∗
    ∃ γg, stbor_stack l 1 (gstk1 ++ GSharedRW γg :: gstk2).
  Proof.
    iIntros (? Hdis) "#BOR Hactive◯".
    iMod (stbor_inv_acc with "BOR") as (stbor) "[Hopen Hclose]"; first done.
    iMod (retag_sharedrw_initial_preserve_ctx with "Hopen Hactive◯") as (γg )"[Hactive◯ Hopen]"; first done.
    iExists γg. iFrame "Hactive◯".
    iApply ("Hclose" with "Hopen").
  Qed.

  (** * Retag (SharedRW) extend *)
  Lemma retag_sharedrw_extend_skip_app tgold tgsnew stklog1 stklog2 stklog2' :
    retag_sharedrw_extend tgold tgsnew stklog2 stklog2' →
    retag_sharedrw_extend tgold tgsnew (stklog1 ++ stklog2) (stklog1 ++ stklog2').
  Proof.
    intros Hretag.
    induction stklog1 as [|itlog1 stklog1 IH]; first apply Hretag.
    rewrite -2!app_comm_cons.
    by apply retag_sharedrw_extend_continue.
  Qed.

  Lemma retag_sharedrw_extend_id tgold tgsnew stklog :
    retag_sharedrw_extend tgold tgsnew stklog stklog.
  Proof.
    induction stklog as [|it stklog]; first apply retag_sharedrw_extend_nil.
    by apply retag_sharedrw_extend_continue.
  Qed.

  Lemma retag_sharedrw_extend_insert tgsnew tgold stklog1 stklog2 tgs itlog2 :
    itlog2 = ItUnique tgold ∨ tgold ∈ tgs →
    retag_sharedrw_extend tgold tgsnew
                          (stklog1 ++ ItSharedRW tgs :: itlog2 :: stklog2)
                          (stklog1 ++ ItSharedRW (tgsnew ∪ tgs) :: itlog2 :: stklog2).
  Proof.
    intros Hgrants.
    apply retag_sharedrw_extend_skip_app.
    apply retag_sharedrw_extend_found.
    - destruct Hgrants as [Hgrants|Hgrants].
      + right. rewrite Hgrants. constructor.
      + left. constructor. assumption.
    - apply retag_sharedrw_extend_id.
  Qed.

  Lemma stbor_ghost_stack_insert_sharedrw tgnew gstk γg tgold stklog1 :
    stbor_grants_retag_sharedrw gstk γg tgold -∗
    stbor_ghost_stack stklog1 gstk ==∗
    ∃ stklog2, ⌜retag_sharedrw_extend tgold {[ tgnew ]} stklog1 stklog2⌝ ∧ stbor_ghost_stack stklog2 gstk ∗ stbor_shared γg tgnew.
  Proof.
    iIntros "#Hgrants Hgstk".
    iDestruct "Hgrants" as (gstk1 git gstk2) "[Hsplit Hgrants]".
    iDestruct "Hsplit" as %Hsplit. rewrite Hsplit.
    iDestruct (stbor_ghost_stack_app_inv_r with "Hgstk") as (stklog11 stklog12 ->) "[Hgstk11 Hgstk12]".
    iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk12") as (itlogrw stklog12' ->) "[Hgit1 Hgstk12]".
    iDestruct (stbor_ghost_stack_cons_inv_r with "Hgstk12") as (itlogu stklog12'' ->) "[Hgit2 Hgstk12]".
    iDestruct "Hgit1" as (tgs Heq) "Hgit". rewrite Heq.
    iAssert (⌜itlogu = ItUnique tgold ∨ tgold ∈ tgs⌝)%I as %Hgrants.
    { iDestruct "Hgrants" as "[->|Hgrants]".
      - iDestruct "Hgit2" as %->. iPureIntro. by left.
      - iDestruct (stbor_shared_elem with "Hgit Hgrants") as %Helem. iPureIntro. by right. }
    iMod (stbor_shared_extend tgnew with "Hgit") as "[Hgit $]".
    iModIntro.
    iAssert (stbor_ghost_item (ItSharedRW ({[ tgnew ]} ∪ tgs)) (GSharedRW γg))%I with "[Hgit]" as "Hgit1".
    { iExists _. by iFrame "Hgit". }
    iDestruct (stbor_ghost_stack_singleton with "Hgit2") as "Hgit2".
    iDestruct (stbor_ghost_stack_singleton with "Hgit1") as "Hgit1".
    iDestruct (stbor_ghost_stack_app with "Hgit2 Hgstk12") as "Hgstk".
    iDestruct (stbor_ghost_stack_app with "Hgit1 Hgstk") as "Hgstk".
    iDestruct (stbor_ghost_stack_app with "Hgstk11 Hgstk") as "Hgstk".
    iExists _. iFrame "Hgstk". iPureIntro.
    by apply retag_sharedrw_extend_insert.
  Qed.

  Lemma stbor_retag_sharedrw_add_ctx_1 l tgold stbor1 q γg gstk :
    stbor_grants_retag_sharedrw gstk γg tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk -∗
    ⌜∃ tgnew stbor2, stbor_step stbor1 (StborRetagEv l tgold [InsideUnsafeCell] (PkRef Immutable) tgnew) stbor2⌝.
  Proof.
    iIntros "#Hgrants Hctx Hactive◯".
    iDestruct "Hctx" as (gstks) "[Hrel Hactive●]".
    iDestruct (stbor_stack_lookup with "Hactive● Hactive◯") as %Hlookup2.
    iDestruct (big_sepM2_lookup_2 with "Hrel") as (stk Hlookup1) "Hrel"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_retag_sharedrw_log with "Hgrants Hgstk") as %Hgrantslog.
    set (tgsnew := {[ Tagged $ stbor_pnext stbor1 ]} : gset tag).
    edestruct (retag_sharedrw_single_step tgsnew) as [stk' Hstep]; [done..|].
    iPureIntro.
    eexists (Tagged $ stbor_pnext stbor1), (MkStborState _ _). constructor.
    by rewrite /do_retag /range_interior_update Hlookup1 /= /retag_single /= Hstep.
  Qed.

  Lemma retag_sharedrw_inv l tgold tgnew stbor1 stbor2 :
    stbor_step stbor1 (StborRetagEv l tgold [InsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    ∃ stkold stknew,
      stack_lookup l stbor1 = Some stkold ∧
      retag_sharedrw_single tgold {[ tgnew ]} stkold = Some stknew ∧
      stbor2 = stack_tick (stack_set l stknew stbor1).
  Proof.
    intros Hstep.
    inversion Hstep as [| | | |? ? ? ? ? ? Hretag].
    rewrite /do_retag /range_update /range_interior_update in Hretag.
    apply bind_Some in Hretag as (stksnew' & Hretag & Htag).
    apply bind_Some in Hretag as (stkold & Hlookup & Hretag).
    apply bind_Some in Hretag as (stknew & Hretag & Hset).
    exists stkold, stknew.
    rewrite /retag_single /= in Hretag.
    inversion Htag.
    repeat split; [done..|].
    destruct stbor1 as [stks1 pnext1]. destruct stbor2 as [stks2 pnext2].
    rewrite /stack_tick /stack_set.
    inversion Hset. subst. by f_equal.
  Qed.

  Lemma stbor_retag_sharedrw_add_ctx l tgold tgnew stbor1 stbor2 q γg gstk :
    stbor_step stbor1 (StborRetagEv l tgold [InsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    stbor_grants_retag_sharedrw gstk γg tgold -∗
    stbor_ctx stbor1 -∗
    stbor_stack l q gstk ==∗

    stbor_ctx stbor2 ∗
    stbor_stack l q gstk ∗
    stbor_shared γg tgnew.
  Proof.
    iIntros (Hstep) "#Hgrants Hctx Hactive◯".
    edestruct retag_sharedrw_inv as (stkold & stknew & Hlookup & Hretag & Hset); first done.
    iDestruct (stbor_ctx_insert_acc_1 with "Hctx Hactive◯") as "(Hrel & Hactive◯ & Hback)"; first done.
    iDestruct "Hrel" as (stklog Hrel) "Hgstk".
    iDestruct (stbor_grants_retag_sharedrw_log with "Hgrants Hgstk") as %Hgrantslog.
    iMod (stbor_ghost_stack_insert_sharedrw with "Hgrants Hgstk") as (stklog2 Hretagrw) "[Hgstk $]".
    iModIntro. iFrame "Hactive◯".
    rewrite Hset.
    iApply "Hback".
    iExists _. iFrame "Hgstk". iPureIntro.
    by eapply retag_sharedrw_single_extend.
  Qed.

  Lemma stbor_retag_sharedrw_add l tgold tgnew stbor1 stbor2 q γg gstk E :
    ↑stborN ⊆ E →
    stbor_step stbor1 (StborRetagEv l tgold [InsideUnsafeCell] (PkRef Immutable) tgnew) stbor2 →
    stbor_inv -∗
    stbor_grants_retag_sharedrw gstk γg tgold -∗
    stbor_interp_auth stbor1 -∗
    stbor_stack l q gstk ={E}=∗

    stbor_interp_auth stbor2 ∗
    stbor_stack l q gstk ∗
    stbor_shared γg tgnew.
  Proof.
    iIntros (? Hstep) "#BOR #Hgrants Hctx Hactive◯".
    iMod (stbor_inv_set stbor2 with "BOR Hctx") as "(Hinterp & Hctx & Hclose)"; first done.
    iMod (stbor_retag_sharedrw_add_ctx with "Hgrants Hinterp Hactive◯") as "[Hinterp Hactive◯]"; try done.
    iFrame "Hactive◯ Hctx".
    iApply ("Hclose" with "Hinterp").
  Qed.

End defs.
