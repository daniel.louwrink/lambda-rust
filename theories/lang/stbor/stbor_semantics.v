From stdpp Require Export gmap fin_maps countable.
From iris.algebra Require Export ofe.
From lrust.lang Require Export lang_base.
Set Default Proof Using "Type".

(** Stacked Borrows definitions *)
Definition ptr_id := nat.

Inductive tag :=
| Untagged
| Tagged (p : ptr_id).

Instance tag_eq_dec : EqDecision tag.
Proof. solve_decision. Defined.

Instance tag_countable : Countable tag.
Proof.
  refine (inj_countable
            (λ tg, match tg with
                   | Tagged t => inl t
                   | Untagged => inr ()
                   end)
            (λ s, match s with
                  | inl t => Some (Tagged t)
                  | inr _ => Some Untagged
                  end) _); by intros [].
Defined.

Inductive item :=
| ItUnique (tg : tag)
| ItDisabled
| ItSharedRW (tgs : gset tag)
| ItSharedRO (tgs : gset tag).

Definition stack := list item.

Definition stacks := gmap loc stack.

Record stbor_state := MkStborState {
  stbor_stacks : stacks;
  stbor_pnext : ptr_id;
}.

(** Stacked Borrows events *)
Inductive mutability := Mutable | Immutable.

Inductive interior_mut := InsideUnsafeCell | OutsideUnsafeCell.

Inductive pointer_kind := PkRef (mut : mutability)
                        | PkRaw (mut : mutability).

Inductive stbor_event :=
| StborAllocEv (l : loc) (sz : nat) (tgnew : tag)
| StborDeallocEv (l : loc) (tg : tag) (sz : nat)
| StborReadEv (l : loc) (tg : tag) (sz : nat)
| StborWriteEv (l : loc) (tg : tag) (sz : nat)
| StborRetagEv (l : loc) (tgold : tag) (interior : list interior_mut) (pkind : pointer_kind) (tgnew : tag).

Instance mutability_dec_eq : EqDecision mutability.
Proof. solve_decision. Defined.

Instance interior_mut_dec_eq : EqDecision interior_mut.
Proof. solve_decision. Defined.

Instance pointer_kind_dec_eq : EqDecision pointer_kind.
Proof. solve_decision. Defined.

(** Stacked Borrows operations *)
Inductive item_grants_write (tg : tag) : item → Prop :=
| item_grants_write_unique : item_grants_write tg (ItUnique tg)
| item_grants_write_sharedrw tgs : tg ∈ tgs → item_grants_write tg (ItSharedRW tgs).
Instance item_grants_write_dec tg it : Decision (item_grants_write tg it).
Proof.
  destruct it as [tgit| |tgsit|tgsit].
  - destruct (decide (tg = tgit)) as [H|H].
    + left. rewrite H. constructor.
    + right. inversion 1. contradiction.
  - right. inversion 1.
  - destruct (decide (tg ∈ tgsit)) as [H|H].
    + left. by constructor.
    + right. inversion 1. contradiction.
  - right. inversion 1.
Qed.

Inductive item_grants_read (tg : tag) : item → Prop :=
| item_grants_read_unique : item_grants_read tg (ItUnique tg)
| item_grants_read_sharedrw tgs : tg ∈ tgs → item_grants_read tg (ItSharedRW tgs)
| item_grants_read_sharedro tgs : tg ∈ tgs → item_grants_read tg (ItSharedRO tgs).
Instance item_grants_read_dec tg it : Decision (item_grants_read tg it).
Proof.
  destruct it as [tgit| |tgsit|tgsit].
  - destruct (decide (tg = tgit)) as [H|H].
    + left. rewrite H. constructor.
    + right. inversion 1. contradiction.
  - right. inversion 1.
  - destruct (decide (tg ∈ tgsit)) as [H|H].
    + left. by constructor.
    + right. inversion 1. contradiction.
  - destruct (decide (tg ∈ tgsit)) as [H|H].
    + left. by constructor.
    + right. inversion 1. contradiction.
Qed.

Inductive item_skippable_read : item → Prop :=
| item_skippable_disabled : item_skippable_read ItDisabled
| item_skippable_read_sharedro tgs : item_skippable_read (ItSharedRO tgs)
| item_skippable_read_sharedrw tgs : item_skippable_read (ItSharedRW tgs).
Instance item_skippable_read_dec it : Decision (item_skippable_read it).
Proof. destruct it as [tgit| |tgsit|tgsit]; (left; econstructor) || (right; inversion 1). Qed.

(* Allocation *)
Definition initial_stack (tg : tag) : stack := [ItUnique tg].

Fixpoint do_init (l : loc) (n : nat) (tg : tag) (stks : stacks) : stacks :=
  match n with
  | O => stks
  | S n => <[l := initial_stack tg]>(do_init (l +ₗ 1) n tg stks)
  end.

(* Writing *)
Fixpoint range_update (l : loc) (n : nat) (dealloc : bool) (f : stack → option stack) (stks : stacks) : option stacks :=
  match n with
  | O => Some stks
  | S n =>
      stkold ← stks !! l;
      stknew ← f stkold;
      if dealloc
      then delete l <$> range_update (l +ₗ 1) n dealloc f stks
      else <[l := stknew]> <$> range_update (l +ₗ 1) n dealloc f stks
  end.

Fixpoint write_single (tg : tag) (stk : stack) : option stack :=
  match stk with
  | [] => None
  | it :: rest => if decide (item_grants_write tg it)
                then Some stk
                else write_single tg rest
  end.

Definition do_write (l : loc) (tg : tag) (n : nat) : stacks → option stacks :=
  range_update l n false (write_single tg).

(* Reading *)
Definition read_traverse_item (it : item) (stk : stack) : stack :=
  if decide (item_skippable_read it)
  then it :: stk
  else ItDisabled :: stk.

Fixpoint read_single (tg : tag) (stkold : stack) : option stack :=
  match stkold with
  | [] => None
  | it :: stkrest =>
    if decide (item_grants_read tg it)
    then Some stkold
    else (* non-matching Unique items are set to Disabled, the other items are kept *)
      read_traverse_item it <$> read_single tg stkrest
  end.

Definition do_read (l : loc) (tg : tag) (n : nat) : stacks → option stacks :=
  range_update l n false (read_single tg).

(* Deallocation *)
(* When deallocating, perform a write access for tg, and then deallocate the stack. *)
Definition do_dealloc (l : loc) (tg : tag) (n : nat) : stacks → option stacks :=
  range_update l n true (write_single tg).

(* Retagging *)
Definition retag_unique_single (tgold tgnew : tag) (stkold : stack) : option stack :=
  stknew ← write_single tgold stkold;
  Some (ItUnique tgnew :: stknew).

Definition merge_push (it : item) (stk : stack) : stack :=
  match it, stk with
  | ItSharedRW tgs1, (ItSharedRW tgs2 :: stkrest) => ItSharedRW (tgs1 ∪ tgs2) :: stkrest
  | ItSharedRO tgs1, (ItSharedRO tgs2 :: stkrest) => ItSharedRO (tgs1 ∪ tgs2) :: stkrest
  | _, _ => it :: stk
  end.

Definition retag_sharedro_single (tgold : tag) (tgsnew : gset tag) (stkold : stack) : option stack :=
  stknew ← read_single tgold stkold;
  Some (merge_push (ItSharedRO tgsnew) stknew).

(* Inserts the SharedRW above *every* granting item, without popping off anything *)
Fixpoint retag_sharedrw_single' (tgold : tag) (tgsnew : gset tag) (stkold : stack) : stack :=
  match stkold with
  | [] => []
  | it :: stkrest =>
    if decide (item_grants_write tgold it)
    then merge_push (ItSharedRW tgsnew) (merge_push it (retag_sharedrw_single' tgold tgsnew stkrest))
    else merge_push it (retag_sharedrw_single' tgold tgsnew stkrest)
  end.

(* Check that there is a write-granting item for `tgold`, then add
`SharedRW(tgsnew)` above *every* granting item. *)
Definition retag_sharedrw_single (tgold : tag) (tgsnew : gset tag) (stkold : stack) : option stack :=
  _ ← write_single tgold stkold;
  Some (retag_sharedrw_single' tgold tgsnew stkold).

Definition new_tag (tgfresh : ptr_id) (pkind : pointer_kind) : tag :=
  match pkind with
  | PkRef _ => Tagged tgfresh
  | PkRaw _ => Untagged
  end.

Definition new_item (tgnew : tag) (pkind : pointer_kind) (int_mut : interior_mut) : item :=
  match pkind, int_mut with
  | PkRef Mutable, _ => ItUnique tgnew
  | PkRef Immutable, OutsideUnsafeCell => ItSharedRO {[ tgnew ]}
  | PkRef Immutable, InsideUnsafeCell => ItSharedRW {[ tgnew ]}
  | PkRaw Mutable, _ => ItSharedRW {[ tgnew ]}
  | PkRaw Immutable, OutsideUnsafeCell => ItSharedRO {[ tgnew ]}
  | PkRaw Immutable, InsideUnsafeCell => ItSharedRW {[ tgnew ]}
  end.

Definition retag_single (tgold tgnew : tag) (pkind : pointer_kind) (int_mut : interior_mut) (stk : stack) : option stack :=
  match new_item tgnew pkind int_mut with
  | ItUnique tgnew => retag_unique_single tgold tgnew stk
  | ItSharedRW tgsnew => retag_sharedrw_single tgold tgsnew stk
  | ItSharedRO tgsnew => retag_sharedro_single tgold tgsnew stk
  | ItDisabled => Some stk
  end.

Fixpoint range_interior_update (l : loc) (interior : list interior_mut) (f : interior_mut → stack → option stack) (stks : stacks) : option stacks :=
  match interior with
  | [] => Some stks
  | int_mut :: interior =>
      stkold ← stks !! l;
      stknew ← f int_mut stkold;
      <[l := stknew]> <$> range_interior_update (l +ₗ 1) interior f stks
  end.

Definition do_retag (l : loc) (tgold : tag) (interior : list interior_mut) (tgfresh : ptr_id) (pkind : pointer_kind) (stksold : stacks) : option (stacks * tag) :=
  let tgnew := new_tag tgfresh pkind in
  stksnew ← range_interior_update l interior (retag_single tgold tgnew pkind) stksold;
  Some (stksnew, tgnew).

(** Stacked Borrows stepping relation *)
Inductive stbor_step' (stksold : stacks) (pnext : ptr_id) : stbor_event → stacks → ptr_id → Prop :=
| StborStepAlloc l sz :
    stbor_step' stksold pnext
                (StborAllocEv l sz (Tagged pnext))
                (do_init l sz (Tagged pnext) stksold) (S pnext)
| StborStepWrite l tg sz stksnew :
    do_write l tg sz stksold = Some stksnew →
    stbor_step' stksold pnext
                (StborWriteEv l tg sz)
                stksnew pnext
| StborStepRead l tg sz stksnew :
    do_read l tg sz stksold = Some stksnew →
    stbor_step' stksold pnext
                (StborReadEv l tg sz)
                stksnew pnext
| StborStepDealloc l tg sz stksnew :
    do_dealloc l tg sz stksold = Some stksnew →
    stbor_step' stksold pnext
                (StborDeallocEv l tg sz)
                stksnew pnext
| StborStepRetag l tgold interior pkind stksnew tgnew :
    do_retag l tgold interior pnext pkind stksold = Some (stksnew, tgnew) →
    stbor_step' stksold pnext
                (StborRetagEv l tgold interior pkind tgnew)
                stksnew (S pnext).

Definition stbor_step (stbor1 : stbor_state) (ev : stbor_event) (stbor2 : stbor_state) : Prop :=
  stbor_step' stbor1.(stbor_stacks) stbor1.(stbor_pnext) ev stbor2.(stbor_stacks) stbor2.(stbor_pnext).
