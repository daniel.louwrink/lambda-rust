From iris.algebra Require Import lib.excl_auth.
From iris.bi Require Import fractional.
From iris.base_logic Require Export lib.own invariants.
From iris.proofmode Require Export tactics.
From lrust.lang.stbor Require Export stbor_semantics stbor_ghost.
From lrust.lang Require Export lang notation lifting heap.
From lrust.lifetime Require Export lifetime_sig lifetime frac_borrow.
Set Default Proof Using "Type".

Section defs.
  Context `{!lrustG Σ, !lftG Σ, !frac_borG Σ}.

  Definition uniq_ref_int' (l : loc) (tg : tag) (gstk : list ghost_item) : iProp Σ :=
    (∃ n : Z, l ↦ #n ∗ stbor_stack l 1 gstk ∗ stbor_grants_write gstk tg).

  Definition uniq_ref_int (κ : lft) (l : loc) (tg : tag) : iProp Σ := ∃ gstk, &{κ} (uniq_ref_int' l tg gstk).

  Lemma reborrow_kill l tgold tgnew git gstk :
    stbor_inv -∗
    stbor_grants_write gstk tgold -∗
    ▷ (▷ uniq_ref_int' l tgnew (git :: gstk) ={↑lft_userN}=∗
    ▷ uniq_ref_int' l tgold gstk).
  Proof.
    iIntros "#BOR #Hgrantsold !> >Href".
    iDestruct "Href" as (n) "(Hl & Hstk & #Hgrantsnew)".
    iMod (stbor_remove gstk with "BOR Hstk") as "Hstk"; first solve_ndisj.
    { by apply suffix_cons_r. }
    iModIntro. iModIntro. iExists n. iFrame "Hl Hstk Hgrantsold".
  Qed.

  (* Unique -> Unique *)
  (* TODO: Maybe use the atomic accessor. It probably requires the "lifetime user"
  namespace to be disjoint from the "lifetime" namespace, though. *)

  (* TODO: How to actually get the stbor_inv invariant from the state
  interpretation here? *)
  Lemma reborrow_uniq_uniq l tgold κ κ' int_mut q E :
    ↑lftN ⊆ E →
    ↑lft_userN ⊆ E →
    lft_ctx -∗
    stbor_inv -∗
    κ' ⊑ κ -∗
    {{{ uniq_ref_int κ l tgold ∗ q.[κ'] }}}
      Retag [int_mut] (PkRef Mutable) (Lit $ LitLoc (l, tgold)) @ E
    {{{ tgnew, RET LitV $ LitLoc (l, tgnew); uniq_ref_int κ' l tgnew ∗ q.[κ'] }}}.
  Proof.
    iIntros (? ?) "#LFT #BOR #Hsublft". iIntros "!>" (Φ) "[Href Hκ'] HΦ".
    iDestruct "Href" as (gstk) "Href".
    iApply fupd_wp.
    iMod (rebor with "LFT Hsublft Href") as "[Href Hrefinh]"; first solve_ndisj.
    iMod (bor_acc_cons with "LFT Href Hκ'") as "[>Href Hcloseref]"; first solve_ndisj.
    iDestruct "Href" as (n) "(Hl & Hstk & #Hgrants)".
    iModIntro. iApply wp_fupd.
    iApply (wp_retag_unique with "Hgrants Hstk"); first solve_ndisj.
    iIntros "!>" (tgnew) "Hstk". iApply "HΦ".
    iSpecialize ("Hcloseref" $! (uniq_ref_int' l tgnew (GUnique tgnew :: gstk))).
    iMod ("Hcloseref" with "[] [Hl Hstk]") as "[Href Hκ']".
    - iApply (reborrow_kill with "BOR Hgrants").
    - iModIntro. iExists n. by iFrame "Hl Hstk".
    - iModIntro. iFrame "Hκ'". iExists (GUnique tgnew :: gstk). iFrame "Href".
  Qed.

  Definition shr_ref_int' (l : loc) (tg : tag) (gstk : ghost_stack) (q : Qp) : iProp Σ :=
    (∃ n : Z, l ↦{q} #n ∗ stbor_stack l q gstk).

  Instance frac_shr_ref_int' l tg gstk : Fractional (shr_ref_int' l tg gstk).
  Proof.
    iIntros (p q); iSplit.
    - iIntros "Href".
      iDestruct "Href" as (n) "[[Hl1 Hl2] [Hactive1 Hactive2]]".
      iSplitL "Hl1 Hactive1".
      + iExists n. iFrame "Hl1 Hactive1".
      + iExists n. iFrame "Hl2 Hactive2".
    - iIntros "[Href1 Href2]".
      iDestruct "Href1" as (n) "[Hl1 Hactive1]".
      iDestruct "Href2" as (m) "[Hl2 Hactive2]".
      iDestruct (heap_mapsto_agree with "[$Hl2 $Hl1]") as %->.
      iCombine "Hl1 Hl2" as "Hl".
      iCombine "Hactive1 Hactive2" as "Hactive".
      iExists n. iFrame "Hl Hactive".
  Qed.

  Definition shr_ref_int (κ : lft) (l : loc) (tg : tag) : iProp Σ :=
    ∃ γg gstk, &frac{κ} (shr_ref_int' l tg (GSharedRO γg :: gstk)) ∗ stbor_grants_read (GSharedRO γg :: gstk) tg.

  Instance pers_shr_ref_int κ l tg : Persistent (shr_ref_int κ l tg).
  Proof. apply _. Qed.

  (* Unique -> Shared *)
  Lemma reborrow_shr_kill l tgold tgnew git gstk :
    stbor_inv -∗
    stbor_grants_write gstk tgold -∗
    ▷ (▷ shr_ref_int' l tgnew (git :: gstk) 1 ={↑lft_userN}=∗
    ▷ uniq_ref_int' l tgold gstk).
  Proof.
    iIntros "#BOR #Hgrantsold !> >Href".
    iDestruct "Href" as (n) "[Hl Hstk]".
    iMod (stbor_remove gstk with "BOR Hstk") as "Hstk"; first solve_ndisj.
    { by apply suffix_cons_r. }
    iModIntro. iModIntro. iExists n. iFrame "Hl Hstk Hgrantsold".
  Qed.

  (* TODO: Move this *)
  Lemma stbor_grants_write_to_read_push_sharedro gstk tg γg :
    stbor_grants_write gstk tg -∗
    stbor_grants_read (GSharedRO γg :: gstk) tg.
  Proof.
    iIntros "#Hgrants".
    destruct gstk as [|[| | |]]; [done| |done| |done].
    - iRight. iApply "Hgrants".
    - iRight. iLeft. iApply "Hgrants".
  Qed.

  Lemma reborrow_uniq_shr l tg κ κ' q E :
    ↑lftN ⊆ E →
    ↑lft_userN ⊆ E →
    lft_ctx -∗
    stbor_inv -∗
    κ' ⊑ κ -∗
    uniq_ref_int κ l tg -∗
    q.[κ'] ={E}=∗

    shr_ref_int κ' l tg ∗
    q.[κ'] ∗
    ([†κ'] ={E}=∗ uniq_ref_int κ l tg).
  Proof.
    iIntros (? ?) "#LFT #BOR #Hsublft Href Hκ'".
    iDestruct "Href" as (gstk) "Href".
    iMod (rebor with "LFT Hsublft Href") as "[Href Hrefinh]"; first solve_ndisj.
    iMod (bor_acc_cons with "LFT Href Hκ'") as "[>Href Hcloseref]"; first solve_ndisj.
    iDestruct "Href" as (n) "(Hl & Hstk & #Hgrants)".
    iDestruct (stbor_grants_write_to_no_sharedro with "Hgrants") as %Hnoshared.
    iMod (stbor_push_sharedro with "BOR Hstk") as (γg) "Hstk"; [solve_ndisj|done|].
    iSpecialize ("Hcloseref" $! (shr_ref_int' l tg (GSharedRO γg :: gstk) 1)).
    iMod ("Hcloseref" with "[] [Hl Hstk]") as "[Href Hκ']".
    - by iApply reborrow_shr_kill.
    - iModIntro. iExists n. iFrame "Hl Hstk".
    - iMod (bor_fracture with "LFT Href") as "Href"; first solve_ndisj. iModIntro.
      iFrame "Hκ'".
      iSplitL "Href".
      + iExists γg, gstk. iFrame "Href".
        iApply stbor_grants_write_to_read_push_sharedro. iFrame "Hgrants".
      + iIntros "Hdead". iMod ("Hrefinh" with "Hdead") as "Href".
        iModIntro. iExists gstk. iFrame "Href".
  Qed.

  (* Shared -> Shared *)
  Lemma reborrow_shr_shr l tgold κ q E :
    ↑lftN ⊆ E →
    ↑lft_userN ⊆ E →
    lft_ctx -∗
    stbor_inv -∗
    shr_ref_int κ l tgold -∗
    {{{ q.[κ] }}}
      Retag [OutsideUnsafeCell] (PkRef Immutable) (Lit $ LitLoc (l, tgold)) @ E
    {{{ tgnew, RET LitV $ LitLoc (l, tgnew); shr_ref_int κ l tgnew ∗ q.[κ] }}}.
  Proof.
    iIntros (? ?) "#LFT #BOR #Href". iIntros "!>" (Φ) "Hκ HΦ".
    iDestruct "Href" as (γg gstk) "[Href #Hgrants]".
    iApply fupd_wp.
    iMod (frac_bor_acc with "LFT Href Hκ") as (q') "[>Hopen Hclose]"; first solve_ndisj.
    iDestruct "Hopen" as (n) "[Hl Hstk]".
    iModIntro. iApply wp_fupd.
    iApply (wp_retag_sharedro_add with "Hgrants Hstk"); first solve_ndisj.
    iIntros "!>" (tgnew) "[Hstk Hshared]".
    iApply "HΦ".
    iMod ("Hclose" with "[Hl Hstk]") as "$".
    { iModIntro. iExists n. iFrame "Hl". iFrame "Hstk". }
    iModIntro. iExists γg, gstk.
    iFrame "Href". iLeft. iFrame "Hshared".
  Qed.

End defs.
