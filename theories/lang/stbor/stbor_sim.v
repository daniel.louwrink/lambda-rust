From stdpp Require Export gmap fin_maps countable.
From iris.algebra Require Export ofe.
From lrust.lang Require Export lang_base.
From lrust.lang.stbor Require Export stbor_semantics.
Set Default Proof Using "Type".

(** * Simulation relation *)
Inductive stack_is_tail_item : item → Prop :=
| stack_is_tail_item_unique tg : stack_is_tail_item (ItUnique tg)
| stack_is_tail_item_disabled : stack_is_tail_item ItDisabled.

Inductive stack_parse_tail : stack → stack → Prop :=
| stack_parse_tail_empty : stack_parse_tail [] []
| stack_parse_tail_shared tgs it stk stk' :
    stack_is_tail_item it →
    stack_parse_tail stk stk' →
    stack_parse_tail (ItSharedRW tgs :: it :: stk) (ItSharedRW tgs :: it :: stk')
| stack_parse_tail_noshared it stk stk' :
    stack_is_tail_item it →
    stack_parse_tail stk stk' →
    stack_parse_tail (it :: stk) (ItSharedRW ∅ :: it :: stk').

Inductive stack_parse : stack → stack → Prop :=
| stack_parse_shared tgs stk stk' :
    stack_parse_tail stk stk' →
    stack_parse (ItSharedRO tgs :: stk) (ItSharedRO tgs :: stk')
| stack_parse_noshared stk stk' :
    stack_parse_tail stk stk' →
    stack_parse stk (ItSharedRO ∅ :: stk').

(* TODO: Maybe simplify this, since stack_parse already ensures that stacks are
of a certain form. We don't have to do the "parsing" twice by also checking here
that SharedRO only appears on top. *)
Inductive stack_sub_unique : item → item → Prop :=
| stack_sub_disabled_disabled : stack_sub_unique ItDisabled ItDisabled
| stack_sub_disabled_unique tg : stack_sub_unique ItDisabled (ItUnique tg)
| stack_sub_unique_unique tg : stack_sub_unique (ItUnique tg) (ItUnique tg).

Inductive stack_sub_tail : stack → stack → Prop :=
| stack_sub_tail_empty : stack_sub_tail [] []
| stack_sub_tail_drop_both stk1 it1 it2 stk2 :
    stack_sub_tail stk1 stk2 →
    stack_sub_tail stk1 (it1 :: it2 :: stk2)
| stack_sub_tail_drop_one it1 stk1 tgs2 it2 stk2 :
    stack_sub_unique it1 it2 →
    stack_sub_tail stk1 stk2 →
    stack_sub_tail (it1 :: stk1) (ItSharedRW tgs2 :: it2 :: stk2)
| stack_sub_tail_keep tgs1 it1 stk1 tgs2 it2 stk2 :
    tgs1 ⊆ tgs2 →
    stack_sub_unique it1 it2 →
    stack_sub_tail stk1 stk2 →
    stack_sub_tail (ItSharedRW tgs1 :: it1 :: stk1) (ItSharedRW tgs2 :: it2 :: stk2).

Inductive stack_sub : stack → stack → Prop :=
| stack_sub_keep_shared tgs1 stk1 tgs2 stk2 :
    tgs1 ⊆ tgs2 →
    stack_sub_tail stk1 stk2 →
    stack_sub (ItSharedRO tgs1 :: stk1) (ItSharedRO tgs2 :: stk2)
| stack_sub_drop_shared stk1 tgs2 stk2 :
    stack_sub_tail stk1 stk2 →
    stack_sub stk1 (ItSharedRO tgs2 :: stk2).

Definition stack_rel_stack (stkphys stklog : stack) : Prop :=
  ∃ stkparsed, stack_parse stkphys stkparsed ∧ stack_sub stklog stkparsed.

(** * Access predicates *)
Inductive stack_log_grants_write (tg : tag) : stack → Prop :=
| stack_log_grants_write_here it stk :
    item_grants_write tg it →
    stack_log_grants_write tg (it :: stk).

Inductive stack_log_grants_read (tg : tag) : stack → Prop :=
| stack_log_grants_read_here it stk :
    item_grants_read tg it →
    stack_log_grants_read tg (it :: stk)
| stack_log_grants_read_skip it stk :
    item_skippable_read it →
    stack_log_grants_read tg stk →
    stack_log_grants_read tg (it :: stk).

Definition stack_log_no_sharedro (stklog : stack) : Prop := ¬ ∃ tgs stklog', stklog = ItSharedRO tgs :: stklog'.

Inductive stack_log_grants_retag_sharedrw (tg : tag) : stack → Prop :=
| stack_log_grants_retag_sharedrw_found tgs it stklog :
    tg ∈ tgs ∨ item_grants_write tg it →
    stack_log_grants_retag_sharedrw tg (ItSharedRW tgs :: it :: stklog)
| stack_log_grants_retag_sharedrw_continue it stklog :
    stack_log_grants_retag_sharedrw tg stklog →
    stack_log_grants_retag_sharedrw tg (it :: stklog).

(** * Logical operations *)
Inductive log_disable_item : item → item → Prop :=
| log_disable_item_disable tg : log_disable_item (ItUnique tg) ItDisabled
| log_disable_item_keep it : log_disable_item it it.

Definition log_disable (stk1 stk2 : stack) : Prop :=
  Forall2 log_disable_item stk1 stk2.

Definition log_pop (stk1 stk2 : stack) : Prop :=
  stk2 `suffix_of` stk1.

Definition retag_sharedrw_initial (stklog stklog1 stklog2 : stack) : Prop :=
  ∃ itlog stklog2',
    stklog = stklog1 ++ stklog2 ∧
    stklog2 = itlog :: stklog2' ∧
    stack_is_tail_item itlog ∧
    ¬ ∃ tgs, last stklog1 = Some (ItSharedRW tgs).

Inductive retag_sharedrw_extend (tgold : tag) (tgsnew : gset tag) : stack → stack → Prop :=
| retag_sharedrw_extend_nil : retag_sharedrw_extend tgold tgsnew [] []
| retag_sharedrw_extend_continue it stklog1 stklog2 :
    retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
    retag_sharedrw_extend tgold tgsnew (it :: stklog1) (it :: stklog2)
| retag_sharedrw_extend_found tgs it stklog1 stklog2 :
    item_grants_write tgold (ItSharedRW tgs) ∨ item_grants_write tgold it →
    retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
    retag_sharedrw_extend tgold tgsnew (ItSharedRW tgs :: it :: stklog1) (ItSharedRW (tgsnew ∪ tgs) :: it :: stklog2).

(** * Preservation properties *)

(** ** Writing *)
Lemma stack_sub_write_skip_sharedro tg tgs stklog stkparsed :
  stack_log_grants_write tg stklog →
  stack_sub stklog (ItSharedRO tgs :: stkparsed) →
  stack_sub_tail stklog stkparsed.
Proof. intros [? ? Hgrants] Hsub; by inversion Hsub in Hgrants; first inversion Hgrants. Qed.

Lemma write_single_drop_item tg it stklog stkparsed stkphys :
  ¬ item_grants_write tg it →
  stack_log_grants_write tg stklog →
  stack_parse_tail (it :: stkphys) stkparsed →
  stack_sub_tail stklog stkparsed →
  ∃ stkparsed', stack_parse_tail stkphys stkparsed' ∧ stack_sub_tail stklog stkparsed'.
Proof.
  intros Hit Hgrants Hparse Hsub.
  destruct Hgrants as [? ? Hgrants].
  inversion Hparse; subst.
  - eexists (ItSharedRW ∅ :: _); split; first by constructor.
    inversion Hsub; subst.
    + by apply stack_sub_tail_drop_both.
    + by apply stack_sub_tail_drop_one.
    + exfalso. apply Hit. constructor. inversion Hgrants. set_solver.
  - eexists stk'; split; first assumption.
    inversion Hsub as [| |? ? ? ? ? Hsubit| ]; subst; first done.
    + inversion Hsubit; subst; [inversion Hgrants|inversion Hgrants|contradiction].
    + inversion Hgrants. set_solver.
Qed.

Lemma write_single_preserve_tail stkparsed1 tg stklog stkphys1 stkphys2 :
   write_single tg stkphys1 = Some stkphys2 →
   stack_log_grants_write tg stklog →
   stack_parse_tail stkphys1 stkparsed1 →
   stack_sub_tail stklog stkparsed1 →
   ∃ stkparsed2, stack_parse_tail stkphys2 stkparsed2 ∧ stack_sub_tail stklog stkparsed2.
Proof.
  intros Hwrite Hgrants Hparse Hsub.
  generalize dependent stkparsed1.
  induction stkphys1 as [|it stkphys1 IH]; intros stkparsed1 Hparse Hsub; first inversion Hwrite.
  rewrite /write_single in Hwrite; case_decide as Hit; rewrite -/write_single in Hwrite.
  - inversion Hwrite. exists stkparsed1; split; done.
  - eapply write_single_drop_item in Hparse as (stkparsed' & Hparse' & Hsub'); [|done..].
    by eapply IH.
Qed.

Lemma stack_rel_from_tail stkphys stklog :
  (∃ stkparsed, stack_parse_tail stkphys stkparsed ∧ stack_sub_tail stklog stkparsed) →
  stack_rel_stack stkphys stklog.
Proof.
  intros (stkparsed & Hparse & Hsub).
  exists (ItSharedRO ∅ :: stkparsed); split.
  - apply stack_parse_noshared. apply Hparse.
  - apply stack_sub_drop_shared. apply Hsub.
Qed.

Lemma write_single_preserve' tg stklog stkphys1 stkphys2 :
  write_single tg stkphys1 = Some stkphys2 →
  stack_log_grants_write tg stklog →
  stack_rel_stack stkphys1 stklog →
  ∃ stkparsed, stack_parse_tail stkphys2 stkparsed ∧ stack_sub_tail stklog stkparsed.
Proof.
  intros Hwrite Hgrants Hrel.
  destruct Hrel as (stkparsed & Hparse & Hsub).
  destruct Hparse.
  - (* SharedRO on top *)
    eapply stack_sub_write_skip_sharedro in Hsub; last done.
    rewrite /write_single in Hwrite;
      case_decide as Hgrants';
      rewrite -/write_single in Hwrite;
      first inversion Hgrants'.
    by eapply write_single_preserve_tail.
  - (* No SharedRO on top *)
    eapply stack_sub_write_skip_sharedro in Hsub; last done.
    by eapply write_single_preserve_tail.
Qed.

Lemma write_single_preserve tg stklog stkphys1 stkphys2 :
  write_single tg stkphys1 = Some stkphys2 →
  stack_log_grants_write tg stklog →
  stack_rel_stack stkphys1 stklog →
  stack_rel_stack stkphys2 stklog.
Proof.
  intros Hwrite Hgrants Hrel.
  eapply stack_rel_from_tail; by eapply write_single_preserve'.
Qed.

(** ** Reading *)

Lemma read_single_sharedrw_inv tg tgs stkphys1 stkphys2 :
  read_single tg (ItSharedRW tgs :: stkphys1) = Some stkphys2 →
  (item_grants_read tg (ItSharedRW tgs) ∧ stkphys2 = ItSharedRW tgs :: stkphys1) ∨
  (∃ stkphys2', ¬ item_grants_read tg (ItSharedRW tgs) ∧ stkphys2 = ItSharedRW tgs :: stkphys2' ∧ read_single tg stkphys1 = Some stkphys2').
Proof.
  intros Hread.
  rewrite /read_single in Hread; case_decide as Hit; rewrite -/read_single in Hread.
  - left. inversion Hread; split; done.
  - right. apply fmap_Some_1 in Hread as (stkphys2' & Hread' & Heq).
    rewrite /read_traverse_item decide_True in Heq; last constructor.
    exists stkphys2'; split; last split; assumption.
Qed.

Lemma stack_is_tail_item_traverse it stk :
  stack_is_tail_item it → read_traverse_item it stk = ItDisabled :: stk.
Proof.
  inversion 1.
  - by rewrite /read_traverse_item decide_False; last inversion 1.
  - by rewrite /read_traverse_item decide_True; last constructor.
Qed.

Lemma read_single_unique_inv tg it stkphys1 stkphys2 :
  stack_is_tail_item it →
  read_single tg (it :: stkphys1) = Some stkphys2 →
  (item_grants_read tg it ∧ stkphys2 = it :: stkphys1) ∨
  (∃ stkphys2', ¬ item_grants_read tg it ∧ stkphys2 = ItDisabled :: stkphys2' ∧ read_single tg stkphys1 = Some stkphys2').
Proof.
  intros Htailit Hread.
  rewrite /read_single in Hread; case_decide as Hit; rewrite -/read_single in Hread.
  - left. inversion Hread; split; done.
  - right. apply fmap_Some_1 in Hread as (stkphys2' & Hread' & Heq).
    rewrite stack_is_tail_item_traverse in Heq; last assumption.
    exists stkphys2'; split; last split; assumption.
Qed.

Lemma read_single_sharedro_inv tg tgs stkphys1 stkphys2 :
  read_single tg (ItSharedRO tgs :: stkphys1) = Some stkphys2 →
  (item_grants_read tg (ItSharedRO tgs) ∧ stkphys2 = ItSharedRO tgs :: stkphys1) ∨
  (∃ stkphys2', ¬ item_grants_read tg (ItSharedRO tgs) ∧ stkphys2 = ItSharedRO tgs :: stkphys2' ∧ read_single tg stkphys1 = Some stkphys2').
Proof.
  intros Hread.
  rewrite /read_single in Hread; case_decide as Hit; rewrite -/read_single in Hread.
  - left. by inversion Hread.
  - right. apply fmap_Some_1 in Hread as (stkphys2' & Hread & Heq).
    rewrite /read_traverse_item decide_True in Heq; last constructor.
    rewrite Heq. by exists stkphys2'.
Qed.

Lemma item_grants_read_not_sharedro tg tgs1 tgs2 :
  tgs1 ⊆ tgs2 →
  ¬ item_grants_read tg (ItSharedRO tgs2) →
  ¬ item_grants_read tg (ItSharedRO tgs1).
Proof. intros Hsub Hit2 Hit1. apply Hit2. constructor. inversion Hit1. set_solver. Qed.

Lemma item_grants_read_not_sharedrw tg tgs1 tgs2 :
  tgs1 ⊆ tgs2 →
  ¬ item_grants_read tg (ItSharedRW tgs2) →
  ¬ item_grants_read tg (ItSharedRW tgs1).
Proof. intros Hsub Hit2 Hit1. apply Hit2. constructor. inversion Hit1. set_solver. Qed.

Lemma item_grants_read_not_unique tg it1 it2 :
  stack_sub_unique it1 it2 →
  ¬ item_grants_read tg it2 →
  ¬ item_grants_read tg it1.
Proof.
  intros Hsub Hit2 Hit1. apply Hit2.
  inversion Hsub; subst.
  - inversion Hit1.
  - inversion Hit1.
  - assumption.
Qed.

Lemma stack_log_grants_drop_not_matching tg it stklog :
  ¬ item_grants_read tg it →
  stack_log_grants_read tg (it :: stklog) →
  stack_log_grants_read tg stklog.
Proof. intros Hit Hgrants. inversion Hgrants; first contradiction. assumption. Qed.

Lemma stack_log_grants_is_disabled_1 tg it1 it2 stklog :
  ¬ item_grants_read tg it2 →
  stack_sub_unique it1 it2 →
  stack_log_grants_read tg (it1 :: stklog) →
  it1 = ItDisabled.
Proof.
  intros Hit Hsubit Hgrants.
  inversion Hgrants as [|? ? Hskip].
  { exfalso. by eapply item_grants_read_not_unique. }
  inversion Hsubit; [done|done|].
  subst. inversion Hskip.
Qed.

Lemma stack_log_grants_is_disabled_2 tg tgs tgs' it1 it2 stklog :
  ¬ item_grants_read tg (ItSharedRW tgs) →
  ¬ item_grants_read tg it2 →
  tgs' ⊆ tgs →
  stack_sub_unique it1 it2 →
  stack_log_grants_read tg (ItSharedRW tgs' :: it1 :: stklog) →
  it1 = ItDisabled.
Proof.
  intros Hit1 Hit2 Htgs Hsubit Hgrants.
  inversion Hgrants.
  { exfalso. by eapply item_grants_read_not_sharedrw. }
  by eapply stack_log_grants_is_disabled_1.
Qed.

Lemma stack_log_grants_split_sharedro tg tgs stklog stkparsed :
  ¬ item_grants_read tg (ItSharedRO tgs) →
  stack_log_grants_read tg stklog →
  stack_sub stklog (ItSharedRO tgs :: stkparsed) →
  ∃ stklog',
    stack_log_grants_read tg stklog' ∧
    stack_sub_tail stklog' stkparsed ∧
    (∀ stkparsed', stack_sub_tail stklog' stkparsed' → stack_sub stklog (ItSharedRO tgs :: stkparsed')).
Proof.
  intros Hit Hgrants Hsub.
  inversion Hsub; subst.
  - apply stack_log_grants_drop_not_matching in Hgrants; last by eapply item_grants_read_not_sharedro.
    eexists; split; last split; [eassumption|eassumption|].
    intros stkparsed' Hsub'. by apply stack_sub_keep_shared.
  - eexists; split; last split; [eassumption|eassumption|].
    intros stkparsed' Hsub'. by apply stack_sub_drop_shared.
Qed.

Lemma stack_log_grants_split_sharedrw tg tgs it stklog stkparsed :
  ¬ item_grants_read tg (ItSharedRW tgs) →
  ¬ item_grants_read tg it →
  stack_log_grants_read tg stklog →
  stack_sub_tail stklog (ItSharedRW tgs :: it :: stkparsed) →
  ∃ stklog',
    stack_log_grants_read tg stklog' ∧
    stack_sub_tail stklog' stkparsed ∧
    (∀ stkparsed', stack_sub_tail stklog' stkparsed' → stack_sub_tail stklog (ItSharedRW tgs :: ItDisabled :: stkparsed')).
Proof.
  intros Hit1 Hit2 Hgrants Hsub.
  inversion Hsub.
  - exists stklog; split; last split; [assumption|assumption|].
    intros stkparsed' Hsub'. apply stack_sub_tail_drop_both. assumption.
  - subst. assert (Hdisabled: it1 = ItDisabled).
    { by eapply stack_log_grants_is_disabled_1; first apply Hit2. }
    rewrite Hdisabled.
    apply stack_log_grants_drop_not_matching in Hgrants; last by eapply item_grants_read_not_unique.
    clear Hsub. eexists; split; last split; [eassumption|eassumption|].
    intros stkparsed' Hsub'. apply stack_sub_tail_drop_one; last assumption.
    constructor.
  - subst. assert (Hdisabled: it1 = ItDisabled).
    { eapply stack_log_grants_is_disabled_2; eauto. }
    rewrite Hdisabled.
    apply stack_log_grants_drop_not_matching in Hgrants; last first.
    { by eapply item_grants_read_not_sharedrw. }
    apply stack_log_grants_drop_not_matching in Hgrants; last first.
    { by eapply item_grants_read_not_unique. }
    clear Hsub. eexists; split; last split; [eassumption|eassumption|].
    intros stkparsed' Hsub'. apply stack_sub_tail_keep; [assumption|constructor|assumption].
Qed.

Lemma read_single_preserve_tail tg stklog stkphys1 stkphys2 stkparsed1 :
  read_single tg stkphys1 = Some stkphys2 →
  stack_log_grants_read tg stklog →
  stack_parse_tail stkphys1 stkparsed1 →
  stack_sub_tail stklog stkparsed1 →
  ∃ stkparsed2, stack_parse_tail stkphys2 stkparsed2 ∧ stack_sub_tail stklog stkparsed2.
Proof.
  intros Hread Hgrants Hparse Hsub.
  generalize dependent stklog.
  generalize dependent stkphys2.
  induction Hparse as [|tgs it stk stk' Htailit Hparse IH|it stk stk' Htailit Hparse IH];
    intros stkphys2 Hread stklog Hgrants Hsub; first inversion Hread.
  - (* SharedRW on top *)
    apply read_single_sharedrw_inv in Hread as [[Hit Heq]|(stkphys2' & Hit & Heq & Hread')].
    { rewrite Heq. eexists. split; last eassumption. by apply stack_parse_tail_shared. }
    apply read_single_unique_inv in Hread' as [[Hit3 Heq3]|(stkphys3 & Hit3 & Heq3 & Hread3)]; last assumption.
    { subst. eexists. split; last eassumption. by apply stack_parse_tail_shared. }
    eapply stack_log_grants_split_sharedrw in Hsub as (stklog' & Hgrants' & Hsub' & Hback); [|eassumption..].
    clear Hgrants. subst.
    edestruct IH as (stkparsed4 & Hparse4 & Hsub4); [eassumption..|].
    eexists; split; last first.
    { apply Hback. apply Hsub4. }
    apply stack_parse_tail_shared; [constructor|assumption].
  - (* No SharedRW on top *)
    apply read_single_unique_inv in Hread as [[Hit3 Heq3]|(stkphys2' & Hit & Heq & Hread')]; last assumption.
    { subst. eexists. split; last eassumption. by apply stack_parse_tail_noshared. }
    eapply stack_log_grants_split_sharedrw in Hsub as (stklog' & Hgrants' & Hsub' & Hback); [| |eassumption..]; last first.
    { inversion 1. set_solver. }
    clear Hgrants. subst.
    edestruct IH as (stkparsed4 & Hparse4 & Hsub4); [eassumption..|].
    eexists; split; last first.
    { apply Hback. apply Hsub4. }
    apply stack_parse_tail_noshared; [constructor|assumption].
Qed.

Lemma read_single_preserve tg stklog stkphys1 stkphys2 :
  read_single tg stkphys1 = Some stkphys2 →
  stack_log_grants_read tg stklog →
  stack_rel_stack stkphys1 stklog →
  stack_rel_stack stkphys2 stklog.
Proof.
  intros Hread Hgrants Hrel.
  destruct Hrel as (stkparsed & Hparse & Hsub).
  destruct stkphys1 as [|it stkphys1]; first inversion Hread.
  inversion Hparse; subst.
  - (* SharedRO on top *)
    apply read_single_sharedro_inv in Hread as [[Hit ->]|(stkphys3 & Hit3 & -> & Hread)]; first by eexists.
    eapply stack_log_grants_split_sharedro in Hsub as (stklog' & Hgrants' & Hsub' & Hback); [|eassumption..].
    edestruct read_single_preserve_tail as (stkparsed2 & Hparse2 & Hsub2); [eassumption..|].
    eexists. split; last eauto.
    apply stack_parse_shared. apply Hparse2.
  - (* No SharedRO on top *)
    eapply stack_log_grants_split_sharedro in Hsub as (stklog' & Hgrants' & Hsub' & Hback); [| |eassumption]; last first.
    { inversion 1. set_solver. }
    edestruct read_single_preserve_tail as (stkparsed2 & Hparse2 & Hsub2); [eassumption..|].
    eexists. split; last eauto.
    apply stack_parse_noshared. apply Hparse2.
Qed.

(** ** Retagging (Unique) *)
Lemma retag_unique_single_preserve tgold tgnew stkphys1 stkphys2 stklog :
  retag_unique_single tgold tgnew stkphys1 = Some stkphys2 →
  stack_log_grants_write tgold stklog →
  stack_rel_stack stkphys1 stklog →
  stack_rel_stack stkphys2 (ItUnique tgnew :: stklog).
Proof.
  intros Hretag Hgrants Hrel.
  rewrite /retag_unique_single /= in Hretag.
  apply bind_Some in Hretag as (stkphys3 & Hwrite & Heq).
  inversion Heq.
  edestruct write_single_preserve' as (stkphys4 & Hparse4 & Hsub4); [eassumption..|].
  eexists; split; last first.
  - apply stack_sub_drop_shared. apply stack_sub_tail_drop_one; [constructor|eassumption].
  - apply stack_parse_noshared. apply stack_parse_tail_noshared; first constructor. apply Hparse4.
Qed.

(** ** Retagging (SharedRO) initial *)
Lemma retag_sharedro_single_initial_preserve stklog stkphys :
  stack_log_no_sharedro stklog →
  stack_rel_stack stkphys stklog →
  stack_rel_stack stkphys (ItSharedRO ∅ :: stklog).
Proof.
  intros Hnoshared Hrel.
  destruct Hrel as (stkparsed & Hparse & Hsub).
  inversion Hparse; subst.
  - eexists; split.
    + apply stack_parse_shared; eassumption.
    + destruct Hsub.
      { exfalso. apply Hnoshared; eauto. }
      apply stack_sub_keep_shared; first set_solver.
      assumption.
  - eexists; split.
    + apply Hparse.
    + apply stack_sub_keep_shared; first set_solver.
      inversion Hsub; subst.
      { exfalso. apply Hnoshared; eauto. }
      assumption.
Qed.

(** ** Retagging (SharedRO) extend *)
Lemma retag_sharedro_single_inv tgold tgsnew stkphys1 stkphys2 :
  retag_sharedro_single tgold tgsnew stkphys1 = Some stkphys2 →
  ∃ stkphys2', stkphys2 = merge_push (ItSharedRO tgsnew) stkphys2' ∧ read_single tgold stkphys1 = Some stkphys2'.
Proof.
  intros Hretag.
  rewrite /retag_sharedro_single in Hretag.
  apply bind_Some in Hretag as (stkphys2' & Hread & Heq2').
  inversion Heq2'.
  eexists; split; done.
Qed.

Lemma merge_push_tail tgs stkphys stkparsed :
  stack_parse_tail stkphys stkparsed →
  merge_push (ItSharedRO tgs) stkphys = ItSharedRO tgs :: stkphys.
Proof.
  intros Hparse.
  destruct Hparse as [| |? ? ? Htailit]; try done.
  inversion Htailit; done.
Qed.

Lemma stack_sub_tail_no_sharedro stklog stkparsed :
  stack_sub_tail stklog stkparsed → ¬ ∃ tgs stklog', stklog = ItSharedRO tgs :: stklog'.
Proof.
  intros Hsub.
  induction Hsub as [|? ? ? ? ? IH|? ? ? ? ? Hsubit|].
  - intros (? & ? & Heq). inversion Heq.
  - apply IH.
  - intros (? & ? & Heq). inversion Hsubit; subst; inversion Heq.
  - intros (? & ? & Heq). inversion Heq.
Qed.

Lemma retag_sharedro_single_extend_preserve tgsnew tgold tgs stkphys1 stkphys2 stklog :
  retag_sharedro_single tgold tgsnew stkphys1 = Some stkphys2 →
  stack_log_grants_read tgold (ItSharedRO tgs :: stklog) →
  stack_rel_stack stkphys1 (ItSharedRO tgs :: stklog) →
  stack_rel_stack stkphys2 (ItSharedRO (tgsnew ∪ tgs) :: stklog).
Proof.
  intros Hretag Hgrants Hrel.
  apply retag_sharedro_single_inv in Hretag as (stkphys2' & Heq & Hread).
  edestruct read_single_preserve as (stkparsed3 & Hparse3 & Hsub3); [done..|].
  inversion Hsub3; subst; last first.
  { exfalso. eapply stack_sub_tail_no_sharedro; [eassumption|eauto]. }
  inversion Hparse3; subst.
  - eexists; split.
    + by apply stack_parse_shared.
    + by apply stack_sub_keep_shared; first set_solver.
  - assert (Heq1: tgs = ∅) by set_solver.
    assert (Heq2: tgsnew ∪ ∅ = tgsnew) by set_solver.
    rewrite Heq1 Heq2.
    erewrite merge_push_tail; last done.
    eexists; split.
    + by apply stack_parse_shared.
    + by apply stack_sub_keep_shared; first set_solver.
Qed.

(** ** Retagging (SharedRW) initial *)

Lemma stack_sub_unique_is_tail_item it1 it2 :
  stack_sub_unique it1 it2 → stack_is_tail_item it1.
Proof. intros Hsub. inversion Hsub; constructor. Qed.

Lemma retag_sharedrw_single_initial_tail stklog stklog1 stklog2 stkparsed :
  retag_sharedrw_initial stklog stklog1 stklog2 →
  stack_sub_tail stklog stkparsed →
  stack_sub_tail (stklog1 ++ ItSharedRW ∅ :: stklog2) stkparsed.
Proof.
  intros Hretag Hsub.
  generalize dependent stklog1.
  induction Hsub as [|? ? ? ? ? IH|? ? ? ? ? ? ? IH|? ? ? ? ? ? ? ? ? IH];
    intros stklog1 Hretag.
  - destruct Hretag as (? & ? & Heq1 & -> & ? & ?).
    apply app_cons_not_nil in Heq1. contradiction.
  - apply stack_sub_tail_drop_both. apply IH. apply Hretag.
  - rewrite /retag_sharedrw_initial in Hretag.
    destruct Hretag as (itlog & stklog2' & Heq1 & -> & Hitlog & Hnoshared).
    destruct stklog1 as [|itlog1 stklog1].
    + simpl in Heq1. simpl.
      inversion Heq1; subst.
      apply stack_sub_tail_keep; [set_solver|assumption..].
    + rewrite -app_comm_cons in Heq1. rewrite -app_comm_cons.
      inversion Heq1; subst.
      apply stack_sub_tail_drop_one; first assumption.
      eapply IH. rewrite /retag_sharedrw_initial.
      do 2 eexists; split; last split; last split; [done|done|done|].
      intros (tgs & Hlast). apply Hnoshared.
      exists tgs. destruct stklog1 as [|]; first inversion Hlast.
      apply Hlast.
  - destruct Hretag as (itlog & stklog2' & Heq1 & -> & Hitlog & Hnoshared).
    destruct stklog1 as [|itlog1 [| itlog2 stklog1]].
    + simpl in Heq1. inversion Heq1; subst.
      inversion Hitlog.
    + simpl in Heq1. inversion Heq1; subst.
      exfalso. apply Hnoshared.
      by eexists.
    + rewrite -2!app_comm_cons in Heq1. inversion Heq1; subst.
      rewrite -2!app_comm_cons. apply stack_sub_tail_keep; [done..|].
      apply IH.
      eexists _, _; split; last split; last split; [done..|].
      intros (tgs & Hlast). apply Hnoshared.
      exists tgs.
      destruct stklog1 as [|]; first inversion Hlast.
      apply Hlast.
Qed.

Lemma retag_sharedrw_initial_inv_sharedro stklog1 tgs stklog2 stklog :
  retag_sharedrw_initial (ItSharedRO tgs :: stklog) stklog1 stklog2 →
  ∃ stklog1', stklog1 = ItSharedRO tgs :: stklog1' ∧ retag_sharedrw_initial stklog stklog1' stklog2.
Proof.
  intros Hretag.
  rewrite /retag_sharedrw_initial in Hretag.
  destruct Hretag as (itlog & stklog2' & Heq1 & -> & Htailit & Hlast).
  destruct stklog1 as [|itlog1 stklog1].
  - simpl in Heq1. inversion Heq1; subst.
    inversion Htailit.
  - rewrite -app_comm_cons in Heq1.
    inversion Heq1; subst.
    eexists; split; first done.
    eexists _, _; split; last split; last split; [done..|].
    intros (tgs' & Hlast'). apply Hlast.
    eexists tgs'. destruct stklog1 as [|]; first inversion Hlast'.
    apply Hlast'.
Qed.

Lemma retag_sharedrw_single_initial stkphys stklog stklog1 stklog2 :
  retag_sharedrw_initial stklog stklog1 stklog2 →
  stack_rel_stack stkphys stklog →
  stack_rel_stack stkphys (stklog1 ++ ItSharedRW ∅ :: stklog2).
Proof.
  intros Hretag (stkparsed & Hparse & Hsub).
  eexists; split; first apply Hparse.
  destruct Hsub.
  - apply retag_sharedrw_initial_inv_sharedro in Hretag as (stklog3 & -> & Hretag).
    apply stack_sub_keep_shared; first done.
    by eapply retag_sharedrw_single_initial_tail.
  - apply stack_sub_drop_shared.
    by eapply retag_sharedrw_single_initial_tail.
Qed.

(** ** Retagging (SharedRW) extend *)
Lemma retag_sharedrw_single_inv tgold tgsnew stkphys1 stkphys2 :
  retag_sharedrw_single tgold tgsnew stkphys1 = Some stkphys2 →
  ∃ stkphys2',
    write_single tgold stkphys1 = Some stkphys2' ∧
    retag_sharedrw_single' tgold tgsnew stkphys1 = stkphys2.
Proof.
  intros Hretag.
  rewrite /retag_sharedrw_single in Hretag.
  apply bind_Some in Hretag as (stkphys2' & Hwrite & Hretag).
  inversion Hretag. eauto.
Qed.

Lemma merge_push_tail_item_l it stk :
  stack_is_tail_item it →
  merge_push it stk = it :: stk.
Proof. by inversion 1. Qed.

Lemma merge_push_tail_item_r it2 it1 stk :
  stack_is_tail_item it2 →
  merge_push it1 (it2 :: stk) = it1 :: it2 :: stk.
Proof. destruct it1; by inversion 1. Qed.

Lemma merge_push_merge_sharedrw tgs1 tgs2 stk :
  merge_push (ItSharedRW tgs1) (merge_push (ItSharedRW tgs2) stk) = merge_push (ItSharedRW (tgs1 ∪ tgs2)) stk.
Proof.
  destruct stk as [|[] stk]; try done.
  simpl. f_equal. f_equal. set_solver.
Qed.

Definition item_either_grants_write (tgold : tag) (tgsrw : gset tag) (ituniq : item) : Prop :=
  item_grants_write tgold (ItSharedRW tgsrw) ∨ item_grants_write tgold ituniq.

Lemma retag_sharedrw_single'_inv_sharedrw tgold tgsnew tgs it stkphys1 stkphys2 :
  stack_is_tail_item it →
  retag_sharedrw_single' tgold tgsnew (ItSharedRW tgs :: it :: stkphys1) = stkphys2 →
  ∃ stkphys2',
    retag_sharedrw_single' tgold tgsnew stkphys1 = stkphys2' ∧
    ((item_either_grants_write tgold tgs it ∧ stkphys2 = ItSharedRW (tgsnew ∪ tgs) :: it :: stkphys2') ∨
     (¬ item_either_grants_write tgold tgs it ∧ stkphys2 = ItSharedRW tgs :: it :: stkphys2')).
Proof.
  intros Htailit Hretag.
  rewrite /retag_sharedrw_single' in Hretag; case_decide as Hit1; rewrite -/retag_sharedrw_single' in Hretag; case_decide as Hit2.
  - rewrite !merge_push_merge_sharedrw (merge_push_tail_item_l it) in Hretag; last done.
    rewrite (merge_push_tail_item_r it) in Hretag; last done.
    rewrite -Hretag. set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    left; split; first by left. f_equal. f_equal. set_solver.
  - rewrite !merge_push_merge_sharedrw (merge_push_tail_item_l it) in Hretag; last done.
    rewrite (merge_push_tail_item_r it) in Hretag; last done.
    rewrite -Hretag. set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    left; split; first by left. done.
  - rewrite !merge_push_merge_sharedrw (merge_push_tail_item_l it) in Hretag; last done.
    rewrite (merge_push_tail_item_r it) in Hretag; last done.
    rewrite -Hretag. set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    left; split; first by right. f_equal. f_equal. set_solver.
  - rewrite (merge_push_tail_item_l it) in Hretag; last done.
    rewrite (merge_push_tail_item_r it) in Hretag; last done.
    rewrite -Hretag. set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    right; split; last done. intros Heither.
    rewrite /item_either_grants_write in Heither. tauto.
Qed.

Lemma retag_sharedrw_single'_inv_unique it tgold tgsnew stkphys1 stkphys2 :
  stack_is_tail_item it →
  retag_sharedrw_single' tgold tgsnew (it :: stkphys1) = stkphys2 →
  ∃ stkphys2',
    retag_sharedrw_single' tgold tgsnew stkphys1 = stkphys2' ∧
    ((item_grants_write tgold it ∧ stkphys2 = ItSharedRW tgsnew :: it :: stkphys2') ∨
     (¬ item_grants_write tgold it ∧ stkphys2 = it :: stkphys2')).
Proof.
  intros Htailit Hretag.
  rewrite /retag_sharedrw_single' in Hretag; case_decide; rewrite -/retag_sharedrw_single' in Hretag.
  - rewrite (merge_push_tail_item_l it) in Hretag; last done.
    rewrite (merge_push_tail_item_r it) in Hretag; last done.
    set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    left; by split.
  - rewrite (merge_push_tail_item_l it) in Hretag; last done.
    set (stkphys2' := retag_sharedrw_single' _ _ _).
    exists stkphys2'; split; first done.
    right; by split.
Qed.


Lemma retag_sharedrw_extend_inv_tail_item tgold tgsnew it stklog1 stklog2 :
  stack_is_tail_item it →
  retag_sharedrw_extend tgold tgsnew (it :: stklog1) stklog2 →
  ∃ stklog3, stklog2 = it :: stklog3 ∧ retag_sharedrw_extend tgold tgsnew stklog1 stklog3.
Proof.
  intros Htailit Hretag.
  inversion Hretag; last first.
  { subst. inversion Htailit. }
  eexists; split; done.
Qed.

(* TODO: Get rid of this horrible duplication... *)
Lemma stack_sub_tail_split_sharedrw1 stklog1 stklog2 tgold tgs tgsnew it stkparsed :
  retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
  stack_sub_tail stklog1 (ItSharedRW tgs :: it :: stkparsed) →
  ∃ stklog1' stklog2',
    retag_sharedrw_extend tgold tgsnew stklog1' stklog2' ∧
    stack_sub_tail stklog1' stkparsed ∧
    (∀ stkparsed', stack_sub_tail stklog2' stkparsed' → stack_sub_tail stklog2 (ItSharedRW (tgsnew ∪ tgs) :: it :: stkparsed')).
Proof.
  intros Hretagl Hsub.
  inversion Hsub as [| |? ? ? ? ? Hsubit|]; subst.
  - do 2 eexists; split; last split; [done..|].
    intros stkparsed' Hsub'. by apply stack_sub_tail_drop_both.
  - inversion Hretagl; subst; last inversion Hsubit. clear Hretagl.
    do 2 eexists; split; last split; [done..|].
    intros stkparsed' Hsub'. by apply stack_sub_tail_drop_one.
  - inversion Hretagl as [|? ? ? Hretagl'|]; last first.
    + do 2 eexists; split; last split; [done..|].
      intros stkparsed' Hsub'. apply stack_sub_tail_keep; [set_solver|done..].
    + apply retag_sharedrw_extend_inv_tail_item in Hretagl' as (stklog3 & -> & Hretagl'); last first.
      { by eapply stack_sub_unique_is_tail_item. }
      subst. do 2 eexists; split; last split; [done..|].
      intros stkparsed' Hsub'. apply stack_sub_tail_keep; [set_solver|done..].
Qed.

Lemma stack_sub_unique_item_grants tg it1 it2 :
  stack_sub_unique it1 it2 → item_grants_write tg it1 → item_grants_write tg it2.
Proof.
  intros Hsub Hgrants.
  inversion Hsub; subst; inversion Hgrants; subst.
  assumption.
Qed.

Lemma stack_sub_tail_split_sharedrw2 stklog1 stklog2 tgold tgs tgsnew it stkparsed :
  ¬ item_either_grants_write tgold tgs it →
  retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
  stack_sub_tail stklog1 (ItSharedRW tgs :: it :: stkparsed) →
  ∃ stklog1' stklog2',
    retag_sharedrw_extend tgold tgsnew stklog1' stklog2' ∧
    stack_sub_tail stklog1' stkparsed ∧
    (∀ stkparsed', stack_sub_tail stklog2' stkparsed' → stack_sub_tail stklog2 (ItSharedRW tgs :: it :: stkparsed')).
Proof.
  intros Hit Hretagl Hsub.
  inversion Hsub as [| |? ? ? ? ? Hsubit|]; subst.
  - do 2 eexists; split; last split; [done..|].
    intros stkparsed' Hsub'. by apply stack_sub_tail_drop_both.
  - inversion Hretagl; subst; last inversion Hsubit. clear Hretagl.
    do 2 eexists; split; last split; [done..|].
    intros stkparsed' Hsub'. by apply stack_sub_tail_drop_one.
  - inversion Hretagl as [|? ? ? Hretagl'|? ? ? ? Hgrants]; last first.
    + exfalso. apply Hit. rewrite /item_either_grants_write.
      destruct Hgrants as [Hgrants|Hgrants].
      * left. constructor. inversion Hgrants. set_solver.
      * right. by eapply stack_sub_unique_item_grants.
    + apply retag_sharedrw_extend_inv_tail_item in Hretagl' as (stklog3 & -> & Hretagl'); last first.
      { by eapply stack_sub_unique_is_tail_item. }
      subst. do 2 eexists; split; last split; [done..|].
      intros stkparsed' Hsub'. apply stack_sub_tail_keep; [set_solver|done..].
Qed.

Lemma retag_sharedrw_single_extend_tail tgold tgsnew stkphys1 stkphys2 stkparsed1 stklog1 stklog2 :
  retag_sharedrw_single' tgold tgsnew stkphys1 = stkphys2 →
  retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
  stack_parse_tail stkphys1 stkparsed1 →
  stack_sub_tail stklog1 stkparsed1 →
  ∃ stkparsed2, stack_parse_tail stkphys2 stkparsed2 ∧ stack_sub_tail stklog2 stkparsed2.
Proof.
  intros Hretagp Hretagl Hparse Hsub.
  generalize dependent stklog2.
  generalize dependent stklog1.
  generalize dependent stkphys2.
  induction Hparse as [|tgs it stk stk' Htailit Hparse IH|it stk stk' Htailit Hparse IH];
    intros stkphys2 Hretagp stklog1 Hsub stklog2 Hretagl.
  - simpl in Hretagp. rewrite -Hretagp. exists []; split; first apply stack_parse_tail_empty.
    inversion Hsub in Hretagl. inversion Hretagl. apply stack_sub_tail_empty.
  - apply retag_sharedrw_single'_inv_sharedrw in Hretagp as (stkphys2' & Hretagp & [[Heither Heq]|[Heither Heq]]); last done.
    + eapply stack_sub_tail_split_sharedrw1 in Hsub as (? & ? & ? & ? & Hback); last done.
      clear Hretagl.
      edestruct IH as (? & ? & ?); [eassumption..|].
      rewrite Heq. eexists; split; first by apply stack_parse_tail_shared.
      by apply Hback.
    + eapply stack_sub_tail_split_sharedrw2 in Hsub as (? & ? & ? & ? & Hback); [|done..].
      clear Hretagl.
      edestruct IH as (? & ? & ?); [eassumption..|].
      rewrite Heq. eexists; split; first by apply stack_parse_tail_shared.
      by apply Hback.
  - apply retag_sharedrw_single'_inv_unique in Hretagp as (stkphys2' & Hretagp & [[Heither Heq]|[Heither Heq]]); last done.
    + eapply stack_sub_tail_split_sharedrw1 in Hsub as (? & ? & ? & ? & Hback); last done.
      clear Hretagl.
      edestruct IH as (? & ? & ?); [eassumption..|].
      rewrite Heq. eexists; split; first by apply stack_parse_tail_shared.
      assert (tgsnew = tgsnew ∪ ∅) as -> by set_solver.
      by apply Hback.
    + eapply stack_sub_tail_split_sharedrw2 in Hsub as (? & ? & ? & ? & Hback); last done; last first.
      { intros Hgrants. apply Heither. destruct Hgrants as [Hgrants|Hgrants]; last assumption.
        inversion Hgrants. set_solver. }
      clear Hretagl.
      edestruct IH as (? & ? & ?); [eassumption..|].
      rewrite Heq. eexists; split; first by apply stack_parse_tail_noshared.
      by apply Hback.
Qed.

Lemma merge_push_sharedro_tail tgs stkphys stkparsed :
  stack_parse_tail stkphys stkparsed →
  merge_push (ItSharedRO tgs) stkphys = ItSharedRO tgs :: stkphys.
Proof.
  intros Hparse. inversion Hparse; [done..|].
  by rewrite (merge_push_tail_item_r it); last done.
Qed.

Lemma retag_sharedrw_single'_inv_sharedro tgs tgold tgsnew stkphys1 stkphys2 :
  retag_sharedrw_single' tgold tgsnew (ItSharedRO tgs :: stkphys1) = stkphys2 →
  ∃ stkphys2',
    retag_sharedrw_single' tgold tgsnew stkphys1 = stkphys2' ∧
    stkphys2 = merge_push (ItSharedRO tgs) stkphys2'.
Proof.
  intros Hretag.
  rewrite /retag_sharedrw_single' in Hretag; case_decide as Hit; rewrite -/retag_sharedrw_single' in Hretag.
  { inversion Hit. }
  eexists; split; last first.
  - symmetry in Hretag. rewrite Hretag. done.
  - done.
Qed.

Lemma retag_sharedrw_extend_inv_sharedro tgold tgsnew tgs stklog1 stklog2 :
  retag_sharedrw_extend tgold tgsnew (ItSharedRO tgs :: stklog1) stklog2 →
  ∃ stklog3, stklog2 = ItSharedRO tgs :: stklog3 ∧ retag_sharedrw_extend tgold tgsnew stklog1 stklog3.
Proof. inversion 1; subst; eauto. Qed.

Lemma retag_sharedrw_single_extend tgold tgsnew stkphys1 stkphys2 stklog1 stklog2 :
  retag_sharedrw_single tgold tgsnew stkphys1 = Some stkphys2 →
  retag_sharedrw_extend tgold tgsnew stklog1 stklog2 →
  stack_rel_stack stkphys1 stklog1 →
  stack_rel_stack stkphys2 stklog2.
Proof.
  intros Hretagp Hretagl (stkparsed1 & Hparse1 & Hsub1).
  apply retag_sharedrw_single_inv in Hretagp as (stkphys2' & Hwrite & Hretagp).
  inversion Hparse1; subst stkphys1 stkparsed1; inversion Hsub1.
  - apply retag_sharedrw_single'_inv_sharedro in Hretagp as (stkphys3 & Heq3 & Hretagp).
    subst. apply retag_sharedrw_extend_inv_sharedro in Hretagl as (stklog3 & Heql3 & Hretagl).
    edestruct retag_sharedrw_single_extend_tail as (stkparsed4 & Hparse4 & Hsub4); [done..|].
    erewrite merge_push_sharedro_tail; last done.
    eexists; split; first by apply stack_parse_shared.
    rewrite Heql3. by apply stack_sub_keep_shared.
  - apply retag_sharedrw_single'_inv_sharedro in Hretagp as (stkphys3 & Heq3 & Hretagp).
    subst.
    edestruct retag_sharedrw_single_extend_tail as (stkparsed4 & Hparse4 & Hsub4); [done..|].
    erewrite merge_push_sharedro_tail; last done.
    eexists; split; first by apply stack_parse_shared.
    by apply stack_sub_drop_shared.
  - subst.
    apply retag_sharedrw_extend_inv_sharedro in Hretagl as (stklog3 & Heql3 & Hretagl).
    edestruct retag_sharedrw_single_extend_tail as (stkparsed4 & Hparse4 & Hsub4); [done..|].
    eexists; split; first by apply stack_parse_noshared.
    rewrite Heql3. by apply stack_sub_keep_shared.
  - subst.
    edestruct retag_sharedrw_single_extend_tail as (stkparsed4 & Hparse4 & Hsub4); [done..|].
    eexists; split; first by apply stack_parse_noshared.
    by apply stack_sub_drop_shared.
Qed.

(** ** Logical popping *)
Lemma log_pop_preserve_tail stkparsed stklog1 stklog2 :
  log_pop stklog1 stklog2 →
  stack_sub_tail stklog1 stkparsed →
  stack_sub_tail stklog2 stkparsed.
Proof.
  intros (stkdead & Hsuffix) Hsub.
  generalize dependent stkdead.
  induction Hsub as [|? ? ? ? ? IH|? ? ? ? ? ? ? IH|? ? ? ? ? ? ? ? ? IH]; intros stkdead Hsuffix.
  - symmetry in Hsuffix. apply app_eq_nil in Hsuffix as [-> ->].
    apply stack_sub_tail_empty.
  - apply stack_sub_tail_drop_both. eapply IH. eassumption.
  - destruct stkdead as [|itdead stkdead].
    + simpl in Hsuffix. rewrite -Hsuffix.
      by apply stack_sub_tail_drop_one.
    + rewrite -app_comm_cons in Hsuffix. inversion Hsuffix.
      apply stack_sub_tail_drop_both. eapply IH. eassumption.
  - destruct stkdead as [|itdead1 [|itdead2 stkdead]].
    + simpl in Hsuffix. rewrite -Hsuffix.
      by apply stack_sub_tail_keep.
    + inversion Hsuffix.
      by apply stack_sub_tail_drop_one.
    + inversion Hsuffix.
      apply stack_sub_tail_drop_both. eapply IH. eassumption.
Qed.

Lemma log_pop_prefix_preserve stkphys stklog1 stklog2 :
  log_pop stklog1 stklog2 →
  stack_rel_stack stkphys stklog1 →
  stack_rel_stack stkphys stklog2.
Proof.
  intros (stkdead & Hsuffix) (stkparsed & Hparse & Hsub).
  destruct Hsub.
  - destruct stkdead as [|itdead stkdead].
    + simpl in Hsuffix. rewrite -Hsuffix.
      eexists; split; first apply Hparse.
      by apply stack_sub_keep_shared.
    + rewrite -app_comm_cons in Hsuffix. inversion Hsuffix.
      eexists; split; first apply Hparse.
      apply stack_sub_drop_shared.
      eapply log_pop_preserve_tail; last eassumption.
      by exists stkdead.
  - eexists; split; first apply Hparse.
    apply stack_sub_drop_shared.
    eapply log_pop_preserve_tail; last eassumption.
    by exists stkdead.
Qed.

(** ** Logical disabling *)
Lemma log_disable_cons_inv_l it1 stklog1 stklog2 :
  log_disable (it1 :: stklog1) stklog2 →
  ∃ it2 stklog2', log_disable_item it1 it2 ∧ log_disable stklog1 stklog2' ∧ stklog2 = it2 :: stklog2'.
Proof.
  intros Hdisable.
  rewrite /log_disable in Hdisable.
  eapply Forall2_cons_inv_l. apply Hdisable.
Qed.

Lemma log_disable_preserve_tail stklog2 stkparsed stklog1 :
  log_disable stklog1 stklog2 →
  stack_sub_tail stklog1 stkparsed →
  stack_sub_tail stklog2 stkparsed.
Proof.
  intros Hdisable Hsub.
  generalize dependent stklog2.
  induction Hsub as [|? ? ? ? ? IH|? ? ? ? ? Hsubit ? IH|? ? ? ? ? ? ? Hsubit ? IH]; intros stklog2 Hdisable.
  - inversion Hdisable. apply stack_sub_tail_empty.
  - apply stack_sub_tail_drop_both. apply IH. apply Hdisable.
  - apply log_disable_cons_inv_l in Hdisable as (it3 & stklog3 & Hdisableit & Hdisable & ->).
    apply stack_sub_tail_drop_one.
    { inversion Hdisableit; subst; last assumption. inversion Hsubit. constructor. }
    apply IH. assumption.
  - apply log_disable_cons_inv_l in Hdisable as (it3 & stklog3 & Hdisableit3 & Hdisable & ->).
    apply log_disable_cons_inv_l in Hdisable as (it4 & stklog4 & Hdisableit4 & Hdisable & ->).
    inversion Hdisableit3; subst.
    apply stack_sub_tail_keep; first done.
    { inversion Hdisableit4; subst; last assumption. inversion Hsubit. constructor. }
    apply IH. assumption.
Qed.

Lemma log_disable_preserve stklog2 stkphys stklog1 :
  log_disable stklog1 stklog2 →
  stack_rel_stack stkphys stklog1 →
  stack_rel_stack stkphys stklog2.
Proof.
  intros Hdisable (stkparsed & Hparse & Hsub).
  destruct Hsub.
  - eexists; split; first apply Hparse.
    apply log_disable_cons_inv_l in Hdisable as (it3 & stklog3 & Hdisableit3 & Hdisable & ->).
    inversion Hdisableit3. apply stack_sub_keep_shared; first done.
    by eapply log_disable_preserve_tail.
  - eexists; split; first apply Hparse.
    apply stack_sub_drop_shared.
    by eapply log_disable_preserve_tail.
Qed.

(** * Stepping properties *)
Inductive stack_sub_item : item → item → Prop :=
| stack_sub_item_disabled : stack_sub_item ItDisabled ItDisabled
| stack_sub_item_disabled_unique tg : stack_sub_item ItDisabled (ItUnique tg)
| stack_sub_item_unique tg : stack_sub_item (ItUnique tg) (ItUnique tg)
| stack_sub_item_sharedrw tgs1 tgs2 : tgs1 ⊆ tgs2 → stack_sub_item (ItSharedRW tgs1) (ItSharedRW tgs2)
| stack_sub_item_sharedro tgs1 tgs2 : tgs1 ⊆ tgs2 → stack_sub_item (ItSharedRO tgs1) (ItSharedRO tgs2).

Inductive item_contains (tg : tag) : item → Prop :=
| item_contains_unique : item_contains tg (ItUnique tg)
| item_contains_sharedrw tgs : tg ∈ tgs → item_contains tg (ItSharedRW tgs)
| item_contains_sharedro tgs : tg ∈ tgs → item_contains tg (ItSharedRO tgs).

Lemma item_grants_write_contains tg it :
  item_grants_write tg it → item_contains tg it.
Proof. inversion 1; subst; by constructor. Qed.

Lemma item_grants_read_contains tg it :
  item_grants_read tg it → item_contains tg it.
Proof. inversion 1; subst; by constructor. Qed.

(* TODO: Maybe get rid of stack_sub_unique in favor of stack_sub_item. *)
Lemma stack_sub_unique_item it1 it2 :
  stack_sub_unique it1 it2 →
  stack_sub_item it1 it2.
Proof. intros Hsub; destruct Hsub; constructor. Qed.

Lemma item_grants_read_sub_item tg it1 it2 :
  stack_sub_item it1 it2 →
  item_grants_read tg it1 →
  item_grants_read tg it2.
Proof.
  intros Hsub Hgrants.
  inversion Hsub; subst; inversion Hgrants; subst.
  - assumption.
  - constructor. set_solver.
  - constructor. set_solver.
Qed.

Lemma item_grants_write_sub_item tg it1 it2 :
  stack_sub_item it1 it2 →
  item_grants_write tg it1 →
  item_grants_write tg it2.
Proof.
  intros Hsub Hgrants.
  inversion Hsub; subst; inversion Hgrants; subst.
  - assumption.
  - constructor. set_solver.
Qed.

Lemma stack_sub_item_contains tg it1 it2 :
  stack_sub_item it1 it2 →
  item_contains tg it1 →
  item_contains tg it2.
Proof.
  intros Hsub Hit1.
  inversion Hit1; subst; inversion Hsub; first done; subst; constructor; set_solver.
Qed.

Lemma item_elem_rel_sub_tail it stklog stkparsed :
  stack_sub_tail stklog stkparsed →
  it ∈ stklog →
  ∃ it', it' ∈ stkparsed ∧ stack_sub_item it it'.
Proof.
  intros Hsub Helem.
  induction Hsub as [|? ? ? ? ? IH|? ? ? ? ? ? ? IH|? ? ? ? ? ? ? ? ? IH]; first inversion Helem.
  - apply IH in Helem as (it' & Helem' & Hsub').
    eexists; split; last done.
    apply elem_of_cons. right.
    apply elem_of_cons. right. assumption.
  - apply elem_of_cons in Helem as [->|Helem].
    + eexists; split; last by eapply stack_sub_unique_item.
      apply elem_of_list_further. apply elem_of_list_here.
    + apply IH in Helem as (it' & Helem' & Hsub').
      eexists; split; last done.
      do 2 apply elem_of_list_further. assumption.
  - apply elem_of_cons in Helem as [->|Helem];
      last apply elem_of_cons in Helem as [->|Helem].
    + eexists; split; first apply elem_of_list_here.
      by constructor.
    + eexists; split; last by eapply stack_sub_unique_item.
      apply elem_of_list_further. apply elem_of_list_here.
    + apply IH in Helem as (it' & Helem' & Hsub').
      eexists; split; last done.
      do 2 apply elem_of_list_further. assumption.
Qed.

Lemma item_elem_rel_sub it stklog stkparsed :
  stack_sub stklog stkparsed →
  it ∈ stklog →
  ∃ it', it' ∈ stkparsed ∧ stack_sub_item it it'.
Proof.
  intros Hsub Helem. destruct Hsub.
  - apply elem_of_cons in Helem as [->|Helem].
    + eexists; split; first apply elem_of_list_here.
      by constructor.
    + edestruct item_elem_rel_sub_tail as (it' & Helem' & Hsub'); [done..|].
      eexists; split; last done.
      apply elem_of_list_further. assumption.
  - edestruct item_elem_rel_sub_tail as (it' & Helem' & Hsub'); [done..|].
    eexists; split; last done.
    apply elem_of_list_further. assumption.
Qed.

Lemma item_elem_rel_parse_tail tg it stkparsed stkphys :
  item_contains tg it →
  stack_parse_tail stkphys stkparsed →
  it ∈ stkparsed →
  it ∈ stkphys.
Proof.
  intros Hit Hparse Helem.
  induction Hparse; first inversion Helem.
  - apply elem_of_cons in Helem as [->|Helem];
      last apply elem_of_cons in Helem as [->|Helem].
    + apply elem_of_list_here.
    + apply elem_of_list_further. apply elem_of_list_here.
    + apply elem_of_list_further. apply elem_of_list_further.
      tauto.
  - apply elem_of_cons in Helem as [->|Helem];
      last apply elem_of_cons in Helem as [->|Helem].
    + inversion Hit. set_solver.
    + apply elem_of_list_here.
    + apply elem_of_list_further. tauto.
Qed.

Lemma item_elem_rel_parse tg it stkparsed stkphys :
  item_contains tg it →
  stack_parse stkphys stkparsed →
  it ∈ stkparsed →
  it ∈ stkphys.
Proof.
  intros Hit Hparse Helem.
  destruct Hparse.
  - apply elem_of_cons in Helem as [->|Helem].
    + apply elem_of_list_here.
    + apply elem_of_list_further.
      by eapply item_elem_rel_parse_tail.
  - apply elem_of_cons in Helem as [->|Helem]; first (inversion Hit; set_solver).
    by eapply item_elem_rel_parse_tail.
Qed.

Lemma item_elem_rel tg it stkphys stklog :
  item_contains tg it →
  stack_rel_stack stkphys stklog →
  it ∈ stklog →
  ∃ it', it' ∈ stkphys ∧ stack_sub_item it it'.
Proof.
  intros Hit (stkparsed & Hparse & Hsub) Helem.
  eapply item_elem_rel_sub in Hsub as (it2 & Helem2 & Hgrants2); [|done..].
  eexists; split.
  - eapply item_elem_rel_parse; [|done..].
    by eapply stack_sub_item_contains.
  - assumption.
Qed.

(** ** Writing *)
Lemma write_single_step_elem_grants tg it stkphys1 :
  item_grants_write tg it →
  it ∈ stkphys1 →
  ∃ stkphys2, write_single tg stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Helem.
  destruct (write_single tg stkphys1) as [stkphys2|] eqn:Hwrite; first by eauto.
  exfalso. induction stkphys1 as [|it1 stkphys1 IH]; first inversion Helem.
  apply elem_of_cons in Helem as [->|Helem].
  + rewrite /write_single decide_True in Hwrite; last done.
    inversion Hwrite.
  + rewrite /write_single in Hwrite; case_decide as Hit1; rewrite -/write_single in Hwrite;
      first inversion Hwrite.
    by apply IH.
Qed.

Lemma stack_log_grants_write_elem_grants tg stklog :
  stack_log_grants_write tg stklog →
  ∃ it, it ∈ stklog ∧ item_grants_write tg it.
Proof. destruct 1. eexists; split; last done. apply elem_of_list_here. Qed.

Lemma write_single_step tg stklog stkphys1 :
  stack_log_grants_write tg stklog →
  stack_rel_stack stkphys1 stklog →
  ∃ stkphys2, write_single tg stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Hrel.
  apply stack_log_grants_write_elem_grants in Hgrants as (it & Helem & Hgrants).
  edestruct item_elem_rel as (it' & Helem' & Hsub'); [|done..|].
  { by eapply item_grants_write_contains. }
  eapply item_grants_write_sub_item in Hsub'; last done.
  by eapply write_single_step_elem_grants.
Qed.

(** ** Reading *)
Lemma read_single_step_elem_grants tg it stkphys1 :
  item_grants_read tg it →
  it ∈ stkphys1 →
  ∃ stkphys2, read_single tg stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Helem.
  destruct (read_single tg stkphys1) as [stkphys2|] eqn:Hread; first by eauto.
  exfalso. induction stkphys1 as [|it1 stkphys1 IH]; first inversion Helem.
  apply elem_of_cons in Helem as [->|Helem].
  + rewrite /read_single decide_True in Hread; last done. inversion Hread.
  + rewrite /read_single in Hread; case_decide as Hit1; rewrite -/read_single in Hread.
    * inversion Hread.
    * apply fmap_None in Hread. by apply IH.
Qed.

Lemma stack_log_grants_read_elem_grants tg stklog :
  stack_log_grants_read tg stklog →
  ∃ it, it ∈ stklog ∧ item_grants_read tg it.
Proof.
  intros Hgrants.
  induction Hgrants as [|? ? ? ? (it' & Helem' & Hgrants')].
  + eexists; split; first apply elem_of_list_here. assumption.
  + eexists; split; first apply elem_of_list_further; eassumption.
Qed.

Lemma read_single_step tg stklog stkphys1 :
  stack_log_grants_read tg stklog →
  stack_rel_stack stkphys1 stklog →
  ∃ stkphys2, read_single tg stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Hrel.
  apply stack_log_grants_read_elem_grants in Hgrants as (it & Helem & Hgrants).
  edestruct item_elem_rel as (it' & Helem' & Hsub'); [|done..|].
  { by eapply item_grants_read_contains. }
  eapply item_grants_read_sub_item in Hsub'; last done.
  by eapply read_single_step_elem_grants.
Qed.

(** ** Retagging (Unique) *)
Lemma retag_unique_single_step tgold tgnew stkphys1 stklog :
  stack_log_grants_write tgold stklog →
  stack_rel_stack stkphys1 stklog →
  ∃ stkphys2, retag_unique_single tgold tgnew stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Hrel.
  edestruct write_single_step as [stkphys2' Hwrite]; [done..|].
  eexists. by rewrite /retag_unique_single Hwrite.
Qed.

(** ** Retagging (SharedRO) *)
Lemma retag_sharedro_single_step tgold tgsnew stkphys1 stklog :
  stack_log_grants_read tgold stklog →
  stack_rel_stack stkphys1 stklog →
  ∃ stkphys2, retag_sharedro_single tgold tgsnew stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Hrel.
  edestruct read_single_step as [stkphys2' Hread]; [done..|].
  eexists. by rewrite /retag_sharedro_single Hread.
Qed.

(** ** Retagging (SharedRW) *)
Lemma stack_log_grants_retag_sharedrw_elem_grants tg stklog :
  stack_log_grants_retag_sharedrw tg stklog →
  ∃ it, it ∈ stklog ∧ item_grants_write tg it.
Proof.
  induction 1 as [? ? ? [Hgrants|Hgrants]|? ? ? (it' & Helem' & Hwrite')].
  - eexists; split; first apply elem_of_list_here. by constructor.
  - eexists; split.
    + apply elem_of_list_further. apply elem_of_list_here.
    + assumption.
  - eexists; split; last done.
    by apply elem_of_list_further.
Qed.

Lemma retag_sharedrw_single_step tgsnew tgold stklog1 stkphys1 :
  stack_log_grants_retag_sharedrw tgold stklog1 →
  stack_rel_stack stkphys1 stklog1 →
  ∃ stkphys2, retag_sharedrw_single tgold tgsnew stkphys1 = Some stkphys2.
Proof.
  intros Hgrants Hrel.
  apply stack_log_grants_retag_sharedrw_elem_grants in Hgrants as (it & Helem & Hgrants).
  edestruct item_elem_rel as (it' & Helem' & Hsub'); [|done..|].
  { by eapply item_grants_write_contains. }
  eapply item_grants_write_sub_item in Hsub'; last done.
  edestruct write_single_step_elem_grants as [stkphys2' Hwrite]; [done..|].
  eexists. by rewrite /retag_sharedrw_single Hwrite.
Qed.
